package com.aocore.service;

import com.aocore.awesomo.AwesomO;
import java.util.ServiceLoader;

/**
 *
 * @author derek
 */
public class AOService
{
    private static AOService service;
    private ServiceLoader<AwesomO> loader;

    /**
     * Creates a new instance of DictionaryService
     */
    private AOService()
    {
        loader = ServiceLoader.load(AwesomO.class);
    }

    /**
     * Retrieve the singleton static instance of DictionaryService.
     */
    public static synchronized AOService getInstance()
    {
        if (service == null)
        {
            service = new AOService();
        }
        return service;
    }

    /**
     * Retrieve definitions from the first provider
     * that contains the word.
     */
    public void start()
    {
        try
        {
            System.out.println("---------------------------");
            for (AwesomO awesomO : loader)
            {
                System.out.println(awesomO);
                Thread thread = new Thread(awesomO.getClass().newInstance());
                thread.start();
            }
            System.out.println("---------------------------");
//            Iterator<AwesomO> dictionaries = loader.iterator();
//
  //          while (dictionaries.hasNext())
    //        {
      //          AwesomO ao = dictionaries.next();
        //        Thread t = new Thread(ao);
          //      t.start();
            //}
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
        }
    }
}
