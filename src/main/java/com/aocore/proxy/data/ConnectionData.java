package com.aocore.proxy.data;

import com.aocore.packet.Packet;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SocketChannel;

/**
 *
 * @author derek
 */
public abstract class ConnectionData
{
    public abstract void callback() throws IOException;

    protected ByteBuffer read = ByteBuffer.allocate(1024);
    protected ByteBuffer write = ByteBuffer.allocate(4096);

    public ConnectionData()
    {
        read.order(ByteOrder.LITTLE_ENDIAN);
    }

    public int read(SocketChannel socketChannel) throws IOException
    {
        int readLength = socketChannel.read(read);
        //System.out.println(socketChannel + " read length " + readLength);
        return readLength;
    }

    public synchronized int write(SocketChannel socketChannel) throws IOException
    {
        write.flip();
        int writeLength = socketChannel.write(write);
        write.compact();
        //System.out.println(socketChannel + " write length " + writeLength);
        return writeLength;
    }

    public synchronized void write(byte... data)
    {
        write.put(data);
    }

    public synchronized void writePacket(Packet packet)
    {
        packet.write(write);
    }
}
