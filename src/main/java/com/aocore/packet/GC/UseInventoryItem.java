/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class UseInventoryItem extends GCPacket
{
    protected int itemUID;
    protected int meX;
    protected int meY;

    public UseInventoryItem(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.itemUID = byteBuffer.getInt(1);
        this.meX = byteBuffer.getInt(5);
        this.meY = byteBuffer.getInt(9);
    }

    public UseInventoryItem(int itemUID, int meX, int meY)
    {
        data = new byte[] { 0x20,
            (byte)(itemUID), (byte)(itemUID >>> 8), (byte)(itemUID >>> 16), (byte)(itemUID >>> 24),
            (byte)(meX), (byte)(meX >>> 8), (byte)(meX >>> 16), (byte)(meX >>> 24),
            (byte)(meY), (byte)(meY >>> 8), (byte)(meY >>> 16), (byte)(meY >>> 24),
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("UseInventoryItem").append("\n");
        stringBuilder.append("itemUID: ").append(itemUID).append("\n");
        stringBuilder.append("meX: ").append(meX).append("\n");
        stringBuilder.append("meY: ").append(meY).append("\n");
        return stringBuilder.toString();
    }
}
