/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.d2data.PartyAction;
import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class PartyRequest extends GCPacket
{
    protected PartyAction action;
    protected int playerUID;

    public PartyRequest(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.action = PartyAction.valueOf(data[1]);
        this.playerUID = byteBuffer.getInt(2);
    }

    public PartyRequest(PartyAction action, int playerUID)
    {
        data = new byte[] { 0x5E,
            (byte)(action.getId()),
            (byte)(playerUID), (byte)(playerUID >>> 8), (byte)(playerUID >>> 16), (byte)(playerUID >>> 24)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("PartyRequest").append("\n");
        stringBuilder.append("action: ").append(action).append("\n");
        stringBuilder.append("playerUID: ").append(playerUID).append("\n");
        return stringBuilder.toString();
    }
}
