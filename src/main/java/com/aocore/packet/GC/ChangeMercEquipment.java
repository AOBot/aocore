/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.d2data.EquipmentLocation;
import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class ChangeMercEquipment extends GCPacket
{
    protected EquipmentLocation location;
    protected boolean unequip = false;

    public ChangeMercEquipment(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.location = EquipmentLocation.valueOf(byteBuffer.getShort(1));
        if (this.location != EquipmentLocation.NotApplicable)
            this.unequip = true;
    }

    public ChangeMercEquipment(EquipmentLocation location)
    {
        data = new byte[] { 0x61,
            (byte)(location.getId()), (byte)(location.getId() >>> 8)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("ChangeMercEquipment").append("\n");
        stringBuilder.append("location: ").append(location).append("\n");
        stringBuilder.append("unequip: ").append(unequip).append("\n");
        return stringBuilder.toString();
    }
}
