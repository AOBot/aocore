/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.d2data.PlayerRelationType;
import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class SetPlayerRelation extends GCPacket
{
    protected PlayerRelationType relation;
    protected boolean value;
    protected int uid;

    public SetPlayerRelation(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.relation = PlayerRelationType.valueOf(data[1]);
        this.value =  data[2] == 1 ? true : false;
        this.uid = byteBuffer.getInt(3);
    }

    public SetPlayerRelation(int uid, PlayerRelationType relation, boolean value)
    {
        data = new byte[] { 0x5D,
            (byte)(relation.getId()), (byte)(value ? 1 : 0),
            (byte)(uid), (byte)(uid >>> 8), (byte)(uid >>> 16), (byte)(uid >>> 24)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("SetPlayerRelation").append("\n");
        stringBuilder.append("relation: ").append(relation).append("\n");
        stringBuilder.append("value: ").append(value).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        return stringBuilder.toString();
    }
}
