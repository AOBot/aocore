/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class EmbedItem extends GCPacket
{
    protected int subjectUID;
    protected int objectUID;

    public EmbedItem(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.subjectUID = byteBuffer.getInt(1);
        this.objectUID = byteBuffer.getInt(5);
    }

    public EmbedItem(int subjectUID, int objectUID)
    {
        data = new byte[] { 0x29,
            (byte)(subjectUID), (byte)(subjectUID >>> 8), (byte)(subjectUID >>> 16), (byte)(subjectUID >>> 24),
            (byte)(objectUID), (byte)(objectUID >>> 8), (byte)(objectUID >>> 16), (byte)(objectUID >>> 24)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("EmbedItem").append("\n");
        stringBuilder.append("subjectUID: ").append(subjectUID).append("\n");
        stringBuilder.append("objectUID: ").append(objectUID).append("\n");
        return stringBuilder.toString();
    }
}
