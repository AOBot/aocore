/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.d2data.GameButton;
import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class ClickButton extends GCPacket
{
    protected GameButton button;
    protected short complement;

    public ClickButton(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.button = GameButton.valueOf(byteBuffer.getInt(1));
        this.complement = byteBuffer.getShort(5);
    }

    public ClickButton(GameButton button, short complement)
    {
        data = new byte[] { 0x4F,
            (byte)(button.getId()), (byte)(button.getId() >>> 8), (byte)(button.getId() >>> 16), (byte)(button.getId() >>> 24),
            (byte)(complement), (byte)(complement >>> 8)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("ClickButton").append("\n");
        stringBuilder.append("button: ").append(button).append("\n");
        stringBuilder.append("complement: ").append(complement).append("\n");
        return stringBuilder.toString();
    }
}
