/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.d2data.SkillType;
import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class SetSkillHotkey extends GCPacket
{
    protected SkillType skill;
    protected short slot;
    protected int chargedItemUID;

    public SetSkillHotkey(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.skill = SkillType.valueOf(byteBuffer.getShort(1));
        this.slot = byteBuffer.getShort(3);
        this.chargedItemUID = byteBuffer.getInt(5);
    }

    public SetSkillHotkey(short slot, SkillType skill, int itemUID)
    {
        data = new byte[] { 0x51,
            (byte)(skill.getId()), (byte)(skill.getId() >>> 8),
            (byte)(slot), (byte)(slot >>> 8),
            (byte)(itemUID), (byte)(itemUID >>> 8), (byte)(itemUID >>> 16), (byte)(itemUID >>> 24)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("SetSkillHotkey").append("\n");
        stringBuilder.append("skill: ").append(skill).append("\n");
        stringBuilder.append("slot: ").append(slot).append("\n");
        stringBuilder.append("chargedItemUID: ").append(chargedItemUID).append("\n");
        return stringBuilder.toString();
    }
}
