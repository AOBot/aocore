/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class Ping extends GCPacket
{
    protected int tickCount;

    public Ping(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.tickCount = byteBuffer.getInt(1);
    }

    public Ping(int tickCount, long unknown5)
    {
        data = new byte[] { 0x6D,
            (byte)(tickCount), (byte)(tickCount >>> 8), (byte)(tickCount >>> 16), (byte)(tickCount >>> 24),
            (byte)(unknown5), (byte)(unknown5 >>> 8), (byte)(unknown5 >>> 16), (byte)(unknown5 >>> 24),
            (byte)(unknown5 >>> 32), (byte)(unknown5 >>> 40), (byte)(unknown5 >>> 48), (byte)(unknown5 >>> 56)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("Ping").append("\n");
        stringBuilder.append("tickCount: ").append(tickCount).append("\n");
        return stringBuilder.toString();
    }
}
