/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.d2data.TownFolkMenuItem;
import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class TownFolkMenuSelect extends GCPacket
{
    protected TownFolkMenuItem selection;
    protected int uid;

    public TownFolkMenuSelect(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.selection = TownFolkMenuItem.valueOf(byteBuffer.getInt(1));
        this.uid = byteBuffer.getInt(5);
    }

    public TownFolkMenuSelect(TownFolkMenuItem selection, int uid, int unknown9)
    {
        data = new byte[] { 0x38,
            (byte)(selection.getId()), (byte)(selection.getId() >>> 8), (byte)(selection.getId() >>> 16), (byte)(selection.getId() >>> 24),
            (byte)(uid), (byte)(uid >>> 8), (byte)(uid >>> 16), (byte)(uid >>> 24),
            (byte)(unknown9), (byte)(unknown9 >>> 8), (byte)(unknown9 >>> 16), (byte)(unknown9 >>> 24)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("TownFolkMenuSelect").append("\n");
        stringBuilder.append("selection: ").append(selection).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        return stringBuilder.toString();
    }
}
