/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.d2data.SkillHand;
import com.aocore.d2data.SkillType;
import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class SelectSkill extends GCPacket
{
    protected SkillType skill;
    protected SkillHand hand = SkillHand.Right;
    protected int chargedItemUID;

    public SelectSkill(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.skill = SkillType.valueOf(byteBuffer.getShort(1));
        this.hand = data[4] == 0x80 ? SkillHand.Left : SkillHand.Right; // A bit weird... whats data[3] and why not use same way GS packet for hand?
        this.chargedItemUID = byteBuffer.getInt(5);
        if (this.chargedItemUID == 0xFFFFFFFF)
            this.chargedItemUID = 0;
    }

    public SelectSkill(SkillType skill, SkillHand hand)
    {
        this(skill, hand, 0xFFFFFFFF);
    }

    public SelectSkill(SkillType skill, SkillHand hand, int chargedItemUID)
    {
        data = new byte[] { 0x3C,
            (byte)(skill.getId()), (byte)(skill.getId() >>> 8),
            0, (byte)(hand == SkillHand.Left ? 0x80 : 0),
            (byte)(chargedItemUID), (byte)(chargedItemUID >>> 8), (byte)(chargedItemUID >>> 16), (byte)(chargedItemUID >>> 24),
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("SelectSkill").append("\n");
        stringBuilder.append("skill: ").append(skill).append("\n");
        stringBuilder.append("hand: ").append(hand).append("\n");
        stringBuilder.append("chargedItemUID: ").append(chargedItemUID).append("\n");
        return stringBuilder.toString();
    }
}
