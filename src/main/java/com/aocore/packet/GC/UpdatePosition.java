/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class UpdatePosition extends GCPacket
{
    protected short x;
    protected short y;

    public UpdatePosition(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.x = byteBuffer.getShort(1);
        this.y = byteBuffer.getShort(3);
    }

    public UpdatePosition(short x, short y)
    {
        data = new byte[] { 0x5F,
            (byte)(x), (byte)(x >>> 8),
            (byte)(y), (byte)(y >>> 8)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("UpdatePosition").append("\n");
        stringBuilder.append("x: ").append(x).append("\n");
        stringBuilder.append("y: ").append(y).append("\n");
        return stringBuilder.toString();
    }
}
