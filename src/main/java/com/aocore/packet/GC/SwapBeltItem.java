/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class SwapBeltItem extends GCPacket
{
    protected int oldItemUID;
    protected int newItemUID;

    public SwapBeltItem(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.oldItemUID = byteBuffer.getInt(1);
        this.newItemUID = byteBuffer.getInt(5);
    }

    public SwapBeltItem(int oldItemUID, int newItemUID)
    {
        data = new byte[] { 0x25,
            (byte)(oldItemUID), (byte)(oldItemUID >>> 8), (byte)(oldItemUID >>> 16), (byte)(oldItemUID >>> 24),
            (byte)(newItemUID), (byte)(newItemUID >>> 8), (byte)(newItemUID >>> 16), (byte)(newItemUID >>> 24),
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("SwapBeltItem").append("\n");
        stringBuilder.append("oldItemUID: ").append(oldItemUID).append("\n");
        stringBuilder.append("newItemUID: ").append(newItemUID).append("\n");
        return stringBuilder.toString();
    }
}
