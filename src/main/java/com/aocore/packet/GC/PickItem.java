/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class PickItem extends GCPacket
{
    protected int requestID;
    protected int uid;
    protected boolean toCursor;

    public PickItem(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.requestID = byteBuffer.getInt(1);
        this.uid = byteBuffer.getInt(5);
        this.toCursor = data[9] == 1 ? true : false;
    }

    public PickItem(int uid, boolean toCursor, int requestID)
    {
        data = new byte[] { 0x16,
            (byte)(requestID), (byte)(requestID >>> 8), (byte)(requestID >>> 16), (byte)(requestID >>> 24),
            (byte)(uid), (byte)(uid >>> 8), (byte)(uid >>> 16), (byte)(uid >>> 24),
            (byte)(toCursor ? 1 : 0), 0, 0, 0
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("PickItem").append("\n");
        stringBuilder.append("requestID: ").append(requestID).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("toCursor: ").append(toCursor).append("\n");
        return stringBuilder.toString();
    }
}
