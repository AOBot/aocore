/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.packet.GCPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class SendOverheadMessage extends GCPacket
{
    protected String message;

    public SendOverheadMessage(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.message = ByteConverter.getString(data, 3);
    }

    public SendOverheadMessage(String message)
    {
        if (message == null || message.length() == 0)
            throw new IllegalArgumentException("message");

        data = new byte[6 + message.length()];
        data[0] = 0x14;
        for (int i = 0; i < message.length(); i++)
            data[3 + i] = (byte)message.getBytes()[i];
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("SendOverheadMessage").append("\n");
        stringBuilder.append("message: ").append(message).append("\n");
        return stringBuilder.toString();
    }
}
