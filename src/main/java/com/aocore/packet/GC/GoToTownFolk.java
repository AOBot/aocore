/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.d2data.UnitType;
import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class GoToTownFolk extends GCPacket
{
    protected UnitType unitType;
    protected int uid;
    protected int x;
    protected int y;

    public GoToTownFolk(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.unitType = UnitType.valueOf(byteBuffer.getInt(1));
        this.uid = byteBuffer.getInt(5);
        this.x = byteBuffer.getInt(9);
        this.y = byteBuffer.getInt(13);
    }

    public GoToTownFolk(UnitType unitType, int uid, int x, int y)
    {
        data = new byte[] { 0x59,
            (byte)(unitType.getId()), 0, 0, 0,
            (byte)(uid), (byte)(uid >>> 8), (byte)(uid >>> 16), (byte)(uid >>> 24),
            (byte)(x), (byte)(x >>> 8), (byte)(x >>> 16), (byte)(x >>> 24),
            (byte)(y), (byte)(y >>> 8), (byte)(y >>> 16), (byte)(y >>> 24)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("GoToTownFolk").append("\n");
        stringBuilder.append("unitType: ").append(unitType).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("x: ").append(x).append("\n");
        stringBuilder.append("y: ").append(y).append("\n");
        return stringBuilder.toString();
    }
}
