/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.d2data.TradeType;
import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class SellItem extends GCPacket
{
    protected int dealerUID;
    protected int itemUID;
    protected TradeType tradeType;
    protected int cost;

    public SellItem(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.dealerUID = byteBuffer.getInt(1);
        this.itemUID = byteBuffer.getInt(5);
        this.tradeType = TradeType.valueOf(byteBuffer.getInt(9));
        this.cost = byteBuffer.getInt(13);
    }

    public SellItem(int dealerUID, int itemUID, int cost)
    {
        data = new byte[] { 0x33,
            (byte)(dealerUID), (byte)(dealerUID >>> 8), (byte)(dealerUID >>> 16), (byte)(dealerUID >>> 24),
            (byte)(itemUID), (byte)(itemUID >>> 8), (byte)(itemUID >>> 16), (byte)(itemUID >>> 24),
            4, 0, 0, 0,
            (byte)(cost), (byte)(cost >>> 8), (byte)(cost >>> 16), (byte)(cost >>> 24),
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("SellItem").append("\n");
        stringBuilder.append("dealerUID: ").append(dealerUID).append("\n");
        stringBuilder.append("itemUID: ").append(itemUID).append("\n");
        stringBuilder.append("tradeType: ").append(tradeType).append("\n");
        stringBuilder.append("cost: ").append(cost).append("\n");
        return stringBuilder.toString();
    }
}
