/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.d2data.RepairType;
import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class TownFolkRepair extends GCPacket
{
    protected int dealerUID;
    protected int itemUID;
    protected RepairType repairType;

    public TownFolkRepair(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.dealerUID = byteBuffer.getInt(1);
        this.itemUID = byteBuffer.getInt(5);
        this.repairType = RepairType.valueOf(byteBuffer.getInt(9));
    }

    // Builds a repair all items packet for a town folk NPC.
    public TownFolkRepair(int dealerUID)
    {
        data = new byte[] { 0x35,
            (byte)(dealerUID), (byte)(dealerUID >>> 8), (byte)(dealerUID >>> 16), (byte)(dealerUID >>> 24),
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, (byte)0x80
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("TownFolkRepair").append("\n");
        stringBuilder.append("dealerUID: ").append(dealerUID).append("\n");
        stringBuilder.append("itemUID: ").append(itemUID).append("\n");
        stringBuilder.append("repairType: ").append(repairType).append("\n");
        return stringBuilder.toString();
    }
}
