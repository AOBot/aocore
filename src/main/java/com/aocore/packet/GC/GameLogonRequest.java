/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.d2data.CharacterClass;
import com.aocore.packet.GCPacket;
import com.aocore.proxy.GameProxy;
import com.aocore.proxy.Proxy;
import com.aocore.util.ByteConverter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 *
 * @author derek
 */
public class GameLogonRequest extends GCPacket
{
    protected int d2GShash;
    protected short d2GSToken;
    protected CharacterClass charClass;
    protected int version;
    protected String name;

    public GameLogonRequest(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.d2GShash = byteBuffer.getInt(1);
        this.d2GSToken = byteBuffer.getShort(5);
        this.charClass = CharacterClass.valueOf(data[7]);
        this.version = byteBuffer.getInt(8);
        //12-15 - int Unknown1
        //16-19 - int Unknown2
        //20    - byte Unknown3
        this.name = ByteConverter.getString(data, 21);
    }

    @Override
    public void process(Proxy proxy)
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byte[] hash = new byte[6];

        byteBuffer.position(1);
        byteBuffer.get(hash, 0, 6);

        try
        {
            proxy.setAwesomO(Proxy.ttt.get(Arrays.hashCode(hash)));
            proxy.getAwesomO().setGameProxy((GameProxy) proxy);
            proxy.connectClient(proxy.getAwesomO().getGameAddress());
        }
        catch (IOException e)
        {
            e.printStackTrace(System.err);
        }
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("GameLogonRequest").append("\n");
        stringBuilder.append("d2GShash: ").append(d2GShash).append("\n");
        stringBuilder.append("d2GSToken: ").append(d2GSToken).append("\n");
        stringBuilder.append("charClass: ").append(charClass).append("\n");
        stringBuilder.append("version: ").append(version).append("\n");
        stringBuilder.append("name: ").append(name).append("\n");
        return stringBuilder.toString();
    }
}
