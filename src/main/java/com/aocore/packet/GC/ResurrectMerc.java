/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class ResurrectMerc extends GCPacket
{
    protected int dealerUID;

    public ResurrectMerc(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.dealerUID = byteBuffer.getInt(1);
    }

    public ResurrectMerc(int dealerUID)
    {
        data = new byte[] { 0x62,
            (byte)(dealerUID), (byte)(dealerUID >>> 8), (byte)(dealerUID >>> 16), (byte)(dealerUID >>> 24)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("ResurrectMerc").append("\n");
        stringBuilder.append("dealerUID: ").append(dealerUID).append("\n");
        return stringBuilder.toString();
    }
}
