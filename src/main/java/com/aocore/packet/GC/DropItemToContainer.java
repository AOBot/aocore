/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.d2data.ItemContainerGC;
import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class DropItemToContainer extends GCPacket
{
    protected int uid;
    protected short x;
    protected short y;
    protected ItemContainerGC container;

    public DropItemToContainer(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.uid = byteBuffer.getInt(1);
        this.x = data[5];
        this.y = data[9];
        this.container = ItemContainerGC.valueOf(data[13]);
    }

    public DropItemToContainer(int uid, ItemContainerGC container, short x, short y)
    {
        data = new byte[] { 0x18,
            (byte)(uid), (byte)(uid >>> 8), (byte)(uid >>> 16), (byte)(uid >>> 24),
            (byte)x, 0, 0, 0,
            (byte)y, 0, 0, 0,
            (byte)(container.getId()), 0, 0, 0
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("DropItemToContainer").append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("x: ").append(x).append("\n");
        stringBuilder.append("y: ").append(y).append("\n");
        stringBuilder.append("container: ").append(container).append("\n");
        return stringBuilder.toString();
    }
}
