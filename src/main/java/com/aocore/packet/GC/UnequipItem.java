/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.d2data.EquipmentLocation;
import com.aocore.packet.GCPacket;

/**
 *
 * @author derek
 */
public class UnequipItem extends GCPacket
{
    protected EquipmentLocation location;

    public UnequipItem(byte... data)
    {
        super(data);

        this.location = EquipmentLocation.valueOf(data[1]);
    }

    public UnequipItem(EquipmentLocation location)
    {
        data = new byte[] { 0x1C,
            (byte)(location.getId()), 0
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("UnequipItem").append("\n");
        stringBuilder.append("location: ").append(location).append("\n");
        return stringBuilder.toString();
    }
}
