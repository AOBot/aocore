/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GC;

import com.aocore.packet.GCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class ItemToCube extends GCPacket
{
    protected int itemUID;
    protected int cubeUID;

    public ItemToCube(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.itemUID = byteBuffer.getInt(1);
        this.cubeUID = byteBuffer.getInt(5);
    }

    public ItemToCube(int itemUID, int cubeUID)
    {
        data = new byte[] { 0x2A,
            (byte)(itemUID), (byte)(itemUID >>> 8), (byte)(itemUID >>> 16), (byte)(itemUID >>> 24),
            (byte)(cubeUID), (byte)(cubeUID >>> 8), (byte)(cubeUID >>> 16), (byte)(cubeUID >>> 24)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GCPacket: ").append("ItemToCube").append("\n");
        stringBuilder.append("itemUID: ").append(itemUID).append("\n");
        stringBuilder.append("cubeUID: ").append(cubeUID).append("\n");
        return stringBuilder.toString();
    }
}
