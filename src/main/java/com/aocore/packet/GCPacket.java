package com.aocore.packet;

import com.aocore.packet.GC.*;

/**
 *
 * @author derek
 */
public abstract class GCPacket extends Packet
{
    public GCPacket(byte... data)
    {
        super(data);
    }

    public static GCPacket create(byte... data)
    {
        try
        {
            switch (data[0] & 0xFF)
            {
                case 0x01: return new WalkToLocation(data);
                case 0x02: return new WalkToTarget(data);
                case 0x03: return new RunToLocation(data);
                case 0x04: return new RunToTarget(data);
                case 0x05: return new CastLeftSkill(data);
                case 0x06: return new CastLeftSkillOnTarget(data);
                case 0x07: return new CastLeftSkillOnTargetStopped(data);
                case 0x08: return new RecastLeftSkill(data);
                case 0x09: return new RecastLeftSkillOnTarget(data);
                case 0x0A: return new RecastLeftSkillOnTargetStopped(data);
                case 0x0C: return new CastRightSkill(data);
                case 0x0D: return new CastRightSkillOnTarget(data);
                case 0x0E: return new CastRightSkillOnTargetStopped(data);
                case 0x0F: return new RecastRightSkill(data);
                case 0x10: return new RecastRightSkillOnTarget(data);
                case 0x11: return new RecastRightSkillOnTargetStopped(data);
                case 0x13: return new UnitInteract(data);
                case 0x14: return new SendOverheadMessage(data);
                case 0x15: return new SendMessage(data);
                case 0x16: return new PickItem(data);
                case 0x17: return new DropItem(data);
                case 0x18: return new DropItemToContainer(data);
                case 0x19: return new PickItemFromContainer(data);
                case 0x1A: return new EquipItem(data);
                case 0x1D: return new SwapEquippedItem(data);
                case 0x1C: return new UnequipItem(data);
                case 0x1F: return new SwapContainerItem(data);
                case 0x20: return new UseInventoryItem(data);
                case 0x21: return new StackItems(data);
                case 0x23: return new AddBeltItem(data);
                case 0x24: return new RemoveBeltItem(data);
                case 0x25: return new SwapBeltItem(data);
                case 0x26: return new UseBeltItem(data);
                case 0x27: return new IdentifyItem(data);
                case 0x29: return new EmbedItem(data);
                case 0x2A: return new ItemToCube(data);
                case 0x2F: return new TownFolkInteract(data);
                case 0x30: return new TownFolkCancelInteraction(data);
                case 0x31: return new DisplayQuestMessage(data);
                case 0x32: return new BuyItem(data);
                case 0x33: return new SellItem(data);
                case 0x34: return new CainIdentifyItems(data);
                case 0x35: return new TownFolkRepair(data);
                case 0x36: return new HireMercenary(data);
                case 0x37: return new IdentifyGambleItem(data);
                case 0x38: return new TownFolkMenuSelect(data);
                case 0x3D: return new HoverUnit(data);
                case 0x3A: return new IncrementAttribute(data);
                case 0x3B: return new IncrementSkill(data);
                case 0x3C: return new SelectSkill(data);
                case 0x3F: return new SendCharacterSpeech(data);
                case 0x40: return new RequestQuestLog(data);
                case 0x41: return new Respawn(data);
                case 0x49: return new WaypointInteract(data);
                case 0x4B: return new RequestReassign(data);
                case 0x4F: return new ClickButton(data);
                case 0x50: return new DropGold(data);
                case 0x51: return new SetSkillHotkey(data);
                case 0x58: return new CloseQuest(data);
                case 0x59: return new GoToTownFolk(data);
                case 0x5D: return new SetPlayerRelation(data);
                case 0x5E: return new PartyRequest(data);
                case 0x5F: return new UpdatePosition(data);
                case 0x60: return new SwitchWeapons(data);
                case 0x61: return new ChangeMercEquipment(data);
                case 0x62: return new ResurrectMerc(data);
                case 0x63: return new InventoryItemToBelt(data);
                case 0x66: return new WardenResponse(data);
                case 0x68: return new GameLogonRequest(data);
                case 0x69: return new ExitGame(data);
                case 0x6B: return new EnterGame(data);
                case 0x6D: return new Ping(data);
                default: throw new IllegalArgumentException("invalid GC packet id " + data[0]);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
            GCPacket packet = new Invalid(data);
            System.err.println("GC: " + packet.getBytes());
            return packet;
        }
    }
}
