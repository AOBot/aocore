package com.aocore.packet;

import com.aocore.packet.RS.*;

/**
 *
 * @author derek
 */
public abstract class RSPacket extends Packet
{
    public RSPacket(byte... data)
    {
        super(data);
    }

    public static RSPacket create(byte... data)
    {
        try
        {
            switch (data[2] & 0xFF)
            {
                case 0x01: return new RealmStartupResponse(data);
                case 0x02: return new CharacterCreationResponse(data);
                case 0x03: return new CreateGameResponse(data);
                case 0x04: return new JoinGameResponse(data);
                case 0x05: return new GameList(data);
                case 0x06: return new GameInfo(data);
                case 0x07: return new CharacterLogonResponse(data);
                case 0x0A: return new CharacterDeletionResponse(data);
                case 0x12: return new MessageOfTheDay(data);
                case 0x14: return new GameCreationQueue(data);
                case 0x18: return new CharacterUpgradeResponse(data);
                case 0x19: return new CharacterList(data);
                default:  throw new IllegalArgumentException("invalid RS packet id " + data[0]);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
            RSPacket packet = new Invalid(data);
            System.err.println("RS: " + packet.getBytes());
            return packet;
        }
    }
}
