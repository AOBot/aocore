package com.aocore.packet;

import com.aocore.packet.RC.*;

/**
 *
 * @author derek
 */
public abstract class RCPacket extends Packet
{
    public RCPacket(byte... data)
    {
        super(data);
    }

    public static RCPacket create(byte... data)
    {
        try
        {
            switch (data[2] & 0xFF)
            {
		case 0x01: return new RealmStartupRequest(data);
		case 0x02: return new CharacterCreationRequest(data);
		case 0x03: return new CreateGameRequest(data);
		case 0x04: return new JoinGameRequest(data);
		case 0x05: return new GameListRequest(data);
		case 0x06: return new GameInfoRequest(data);
		case 0x07: return new CharacterLogonRequest(data);
		case 0x0A: return new CharacterDeletionRequest(data);
		case 0x12: return new MessageOfTheDayRequest(data);
		case 0x13: return new CancelGameCreation(data);
		case 0x18: return new CharacterUpgradeRequest(data);
		case 0x19: return new CharacterListRequest(data);
                default: throw new IllegalArgumentException("invalid RC packet id " + data[0]);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
            RCPacket packet = new Invalid(data);
            System.err.println("RC: " + packet.getBytes());
            return packet;
        }
    }
}
