package com.aocore.packet;

import com.aocore.packet.CS.*;

/**
 *
 * @author derek
 */
public abstract class CSPacket extends Packet
{
    public CSPacket(byte... data)
    {
        super(data);
    }

    public static CSPacket create(byte... data)
    {
        try
        {
            switch (data[1] & 0xFF)
            {
                case 0x00: return new KeepAlive(data);
                case 0x0A: return new EnterChatResponse(data);
                case 0x0B: return new ChannelList(data);
                case 0x0F: return new ChatEvent(data);
                case 0x15: return new AdInfo(data);
                case 0x25: return new BnetPing(data);
                case 0x33: return new FileTimeInfo(data);
                case 0x3A: return new BnetLogonResponse(data);
                case 0x3E: return new RealmLogonResponse(data);
                case 0x40: return new QueryRealmsResponse(data);
                case 0x46: return new NewsInfo(data);
                case 0x4A: return new ExtraWorkInfo(data);
                case 0x4C: return new RequiredExtraWorkInfo(data);
                case 0x50: return new BnetConnectionResponse(data);
                case 0x51: return new BnetAuthResponse(data);
                default: throw new IllegalArgumentException("invalid CS packet id " + data[0]);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
            CSPacket packet = new Invalid(data);
            System.err.println("CS: " + packet.getBytes());
            return packet;
        }
    }
}
