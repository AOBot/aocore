package com.aocore.packet;

import com.aocore.packet.CC.*;

/**
 *
 * @author derek
 */
public abstract class CCPacket extends Packet
{
    public CCPacket(byte... data)
    {
        super(data);
    }

    public static CCPacket create(byte... data)
    {
        try
        {
            switch (data[1] & 0xFF)
            {
                case 0x00: return new KeepAlive(data);
                case 0x0A: return new EnterChatRequest(data);
                case 0x0B: return new ChannelListRequest(data);
                case 0x0C: return new JoinChannel(data);
                case 0x0E: return new ChatCommand(data);
                case 0x10: return new LeaveChat(data);
                case 0x15: return new AdInfoRequest(data);
                case 0x1C: return new StartGame(data);
                case 0x1F: return new LeaveGame(data);
                case 0x21: return new DisplayAd(data);
                case 0x22: return new NotifyJoin(data);
                case 0x25: return new BnetPong(data);
                case 0x33: return new FileTimeRequest(data);
                case 0x3A: return new BnetLogonRequest(data);
                case 0x3E: return new RealmLogonRequest(data);
                case 0x40: return new QueryRealms(data);
                case 0x4B: return new ExtraWorkResponse(data);
                case 0x46: return new NewsInfoRequest(data);
                case 0x50: return new BnetConnectionRequest(data);
                case 0x51: return new BnetAuthRequest(data);
                default: throw new IllegalArgumentException("invalid CC packet id " + data[0]);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
            CCPacket packet = new Invalid(data);
            System.err.println("CC: " + packet.getBytes());
            return packet;
        }
    }
}
