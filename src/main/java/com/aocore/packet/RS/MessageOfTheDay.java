/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.RS;

import com.aocore.packet.RSPacket;
import com.aocore.util.ByteConverter;

/**
 *
 * @author derek
 */
public class MessageOfTheDay extends RSPacket
{
    protected String message;

    public MessageOfTheDay(byte... data)
    {
        super(data);

        // unknown : starting bytes before first 0...
        // supposedly some kind of header but sometimes null, otherwise control characters so unlikely to be a string...
        int offset = 3;
        while (data[offset++] != 0) continue;
        this.message = ByteConverter.getString(data, offset);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RSPacket: ").append("MessageOfTheDay").append("\n");
        stringBuilder.append("message: ").append(message).append("\n");
        return stringBuilder.toString();
    }
}
