/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.RS;

import com.aocore.d2data.JoinGameResult;
import com.aocore.packet.RSPacket;
import com.aocore.proxy.Proxy;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 *
 * @author derek
 */
public class JoinGameResponse extends RSPacket
{
    protected short requestID;
    protected short gameToken;
    protected short unknown;
    protected int gameServerIP;
    protected int gameHash;
    protected JoinGameResult result;

    public JoinGameResponse(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.requestID = byteBuffer.getShort(3);
        this.gameToken = byteBuffer.getShort(5);
        this.unknown = byteBuffer.getShort(7);
        this.gameServerIP = byteBuffer.getInt(9);
        this.gameHash = byteBuffer.getInt(13);
        this.result = JoinGameResult.valueOf(byteBuffer.getInt(17));
    }

    @Override
    public void process(Proxy proxy)
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        //byteBuffer.order(ByteOrder.LITTLE_ENDIAN);

        byte[] ip = new byte[] { byteBuffer.get(9), byteBuffer.get(10), byteBuffer.get(11), byteBuffer.get(12) };
        byte[] hash = new byte[6];

        byteBuffer.position(13);
        byteBuffer.get(hash, 0, 4);
        byteBuffer.position(5);
        byteBuffer.get(hash, 4, 2);

        try
        {
            InetSocketAddress address = new InetSocketAddress(InetAddress.getByAddress(ip), 4000);
            proxy.getAwesomO().setGameAddress(address);
            Proxy.ttt.put(Arrays.hashCode(hash), proxy.getAwesomO());

            System.err.println("hash: " + Arrays.hashCode(hash));
            System.err.println("ip: " + address.getHostName());
            System.err.println("port: " + 4000);
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
        }

        byteBuffer.put(9, (byte) 192);
        byteBuffer.put(10, (byte) 168);
        byteBuffer.put(11, (byte) 1);
        byteBuffer.put(12, (byte) 145);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RSPacket: ").append("JoinGameResponse").append("\n");
        stringBuilder.append("requestID: ").append(requestID).append("\n");
        stringBuilder.append("gameToken: ").append(gameToken).append("\n");
        stringBuilder.append("gameServerIP: ").append(gameServerIP).append("\n");
        stringBuilder.append("gameHash: ").append(gameHash).append("\n");
        stringBuilder.append("result: ").append(result).append("\n");
        return stringBuilder.toString();
    }
}
