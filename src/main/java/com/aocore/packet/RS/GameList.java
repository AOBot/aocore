/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.RS;

import com.aocore.d2data.GameFlags;
import com.aocore.packet.RSPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class GameList extends RSPacket
{
    protected short requestID;
    protected int index;
    protected byte playerCount;
    protected GameFlags flags;
    protected String name;
    protected String description = null;

    public GameList(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.requestID = byteBuffer.getShort(3);
        this.index = byteBuffer.getInt(5);
        this.playerCount = data[9];
        this.flags = GameFlags.valueOf(byteBuffer.getInt(10));
        if ((this.flags.getId() & GameFlags.Valid.getId()) != GameFlags.Valid.getId())
            return;
        this.name = ByteConverter.getString(data, 14);
        if (data.length > 16 + this.name.length())
            this.description = ByteConverter.getString(data, 15 + this.name.length());
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RSPacket: ").append("GameList").append("\n");
        stringBuilder.append("requestID: ").append(requestID).append("\n");
        stringBuilder.append("index: ").append(index).append("\n");
        stringBuilder.append("playerCount: ").append(playerCount).append("\n");
        stringBuilder.append("flags: ").append(flags).append("\n");
        stringBuilder.append("name: ").append(name).append("\n");
        stringBuilder.append("description: ").append(description).append("\n");
        return stringBuilder.toString();
    }
}
