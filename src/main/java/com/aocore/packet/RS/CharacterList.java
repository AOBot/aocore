/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.RS;

import com.aocore.d2data.CharacterInfo;
import com.aocore.packet.RSPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;

/**
 *
 * @author derek
 */
public class CharacterList extends RSPacket
{
    protected int requested;
    protected int total;
    protected int listed;
    protected CharacterInfo[] characters;

    public CharacterList(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.requested = byteBuffer.getShort(3);
        this.total = byteBuffer.getInt(5);
        this.listed = byteBuffer.getShort(9);
        this.characters = new CharacterInfo[this.listed];
        int index = 11;
        for (int i=0; i < this.listed && index < data.length; i++)
        {
            this.characters[i] = new CharacterInfo();
            this.characters[i].expires = new Date(byteBuffer.getInt(index));
            this.characters[i].name = ByteConverter.getString(data, (index += 4));
            index += this.characters[i].name.length() + 1;
//            StatString.ParseD2StatString(data, index,
//            ref this.characters[i].ClientVersion, ref this.characters[i].Class,
//            ref this.characters[i].Level, ref this.characters[i].Flags,
//            ref this.characters[i].Act, ref this.characters[i].Title);

            index = ByteConverter.getBytePosition(data, 0, index) + 1;
        }
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RSPacket: ").append("CharacterList").append("\n");
        stringBuilder.append("requested: ").append(requested).append("\n");
        stringBuilder.append("total: ").append(total).append("\n");
        stringBuilder.append("listed: ").append(listed).append("\n");
        stringBuilder.append("characters: ").append(characters).append("\n");
        return stringBuilder.toString();
    }
}
