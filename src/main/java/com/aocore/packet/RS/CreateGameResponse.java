/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.RS;

import com.aocore.d2data.CreateGameResult;
import com.aocore.packet.RSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class CreateGameResponse extends RSPacket
{
    protected short requestID;
    protected CreateGameResult result;
    protected int unknown;

    public CreateGameResponse(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.requestID = byteBuffer.getShort(3);
        this.unknown = byteBuffer.getInt(5);
        this.result = CreateGameResult.valueOf(byteBuffer.getInt(9));
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RSPacket: ").append("CreateGameResponse").append("\n");
        stringBuilder.append("requestID: ").append(requestID).append("\n");
        stringBuilder.append("result: ").append(result).append("\n");
        stringBuilder.append("unknown: ").append(unknown).append("\n");
        return stringBuilder.toString();
    }
}
