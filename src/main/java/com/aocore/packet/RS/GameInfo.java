/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.RS;

import com.aocore.d2data.CharacterBaseInfo;
import com.aocore.d2data.CharacterClass;
import com.aocore.d2data.GameFlags;
import com.aocore.packet.RSPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class GameInfo extends RSPacket
{
    protected short requestID;
    protected GameFlags flags;
    protected int uptime;
    protected int maxPlayers;
    protected int playerCount;
    protected int creatorLevel;
    protected int levelRestriction;
    protected int minLevel = -1;
    protected int maxLevel = -1;
    protected CharacterBaseInfo[] players;

    public GameInfo(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.requestID = byteBuffer.getShort(3);
        this.flags = GameFlags.valueOf(byteBuffer.getInt(5));
        this.uptime = byteBuffer.getInt(9);
        this.creatorLevel = data[13];
        this.levelRestriction = data[14];
        if (data[14] != 0xFF)
        {
            this.minLevel = Math.max(1, data[13] - data[14]);
            this.maxLevel = Math.min(99, data[13] + data[14]);
        }
        this.maxPlayers = data[15];
        this.playerCount = data[16];
        this.players = new CharacterBaseInfo[this.playerCount];
        int namePos = 50;
        for (int i=0; i < this.playerCount; i++)
        {
            this.players[i] = new CharacterBaseInfo(ByteConverter.getString(data, namePos), CharacterClass.valueOf(data[17 + i]), data[33 + i]);
            namePos += this.players[i].getName().length() + 1;
        }
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RSPacket: ").append("GameInfo").append("\n");
        stringBuilder.append("requestID: ").append(requestID).append("\n");
        stringBuilder.append("flags: ").append(flags).append("\n");
        stringBuilder.append("uptime: ").append(uptime).append("\n");
        stringBuilder.append("maxPlayers: ").append(maxPlayers).append("\n");
        stringBuilder.append("playerCount: ").append(playerCount).append("\n");
        stringBuilder.append("creatorLevel: ").append(creatorLevel).append("\n");
        stringBuilder.append("levelRestriction: ").append(levelRestriction).append("\n");
        stringBuilder.append("minLevel: ").append(minLevel).append("\n");
        stringBuilder.append("maxLevel: ").append(maxLevel).append("\n");
        stringBuilder.append("players: ").append(players).append("\n");
        return stringBuilder.toString();
    }
}
