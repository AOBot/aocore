/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.AreaLevel;
import com.aocore.d2data.GameObjectID;
import com.aocore.d2data.GameObjectInteractType;
import com.aocore.d2data.GameObjectMode;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class AssignGameObject extends GSPacket
{
    protected GameObjectID objectID;
    protected GameObjectInteractType interactType;
    protected GameObjectMode objectMode;
    protected int uid;
    protected int x;
    protected int y;
    protected AreaLevel destination;

    public AssignGameObject(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.uid = byteBuffer.getInt(2);
        this.objectID = GameObjectID.valueOf(byteBuffer.getShort(6));
        this.x = byteBuffer.getShort(8);
        this.y = byteBuffer.getShort(10);
        this.objectMode = GameObjectMode.valueOf(data[12]);

        if (this.objectID == GameObjectID.TownPortal)
        {
            this.interactType = GameObjectInteractType.TownPortal;
            this.destination = AreaLevel.valueOf(data[13]);
        }
        else
        {
            this.interactType = GameObjectInteractType.valueOf(data[13]);
            this.destination = AreaLevel.None;
        }
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("AssignGameObject").append("\n");
        stringBuilder.append("objectID: ").append(objectID).append("\n");
        stringBuilder.append("interactType: ").append(interactType).append("\n");
        stringBuilder.append("objectMode: ").append(objectMode).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("x: ").append(x).append("\n");
        stringBuilder.append("y: ").append(y).append("\n");
        stringBuilder.append("destination: ").append(destination).append("\n");
        return stringBuilder.toString();
    }
}
