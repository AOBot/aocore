/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.GameDifficulty;
import com.aocore.packet.GSPacket;

/**
 *
 * @author derek
 */
public class GameLogonReceipt extends GSPacket
{
    protected GameDifficulty difficulty;
    protected boolean hardcore;
    protected boolean expansion;
    protected boolean ladder;

    public GameLogonReceipt(byte... data)
    {
        super(data);

        this.difficulty = GameDifficulty.valueOf(data[1]);
        this.hardcore = (data[3] & 8) == 8 ? true : false;
        this.expansion = data[6] == 1 ? true : false;
        this.ladder = data[7] == 1 ? true : false;
    }

    public GameDifficulty getDifficulty()
    {
        return difficulty;
    }

    public boolean isHardcore()
    {
        return hardcore;
    }

    public boolean isExpansion()
    {
        return expansion;
    }

    public boolean isLadder()
    {
        return ladder;
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("GameLogonReceipt").append("\n");
        stringBuilder.append("difficulty: ").append(difficulty).append("\n");
        stringBuilder.append("hardcore: ").append(hardcore).append("\n");
        stringBuilder.append("expansion: ").append(expansion).append("\n");
        stringBuilder.append("ladder: ").append(ladder).append("\n");
        return stringBuilder.toString();
    }
}
