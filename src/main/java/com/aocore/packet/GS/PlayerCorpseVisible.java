/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class PlayerCorpseVisible extends GSPacket
{
    protected boolean assign;
    protected int playerUID;
    protected int corpseUID;

    public PlayerCorpseVisible(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.assign = data[1] == 1 ? true : false;
        this.playerUID = byteBuffer.getInt(2);
        this.corpseUID = byteBuffer.getInt(6);
    }

    public PlayerCorpseVisible(boolean assign, int playerUID, int corpseUID)
    {
        data = new byte[] { 0x74,
            (byte)(assign ? 1 : 0),
            (byte)(playerUID), (byte)(playerUID >>> 8), (byte)(playerUID >>> 16), (byte)(playerUID >>> 24),
            (byte)(corpseUID), (byte)(corpseUID >>> 8), (byte)(corpseUID >>> 16), (byte)(corpseUID >>> 24)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("PlayerCorpseVisible").append("\n");
        stringBuilder.append("assign: ").append(assign).append("\n");
        stringBuilder.append("playerUID: ").append(playerUID).append("\n");
        stringBuilder.append("corpseUID: ").append(corpseUID).append("\n");
        return stringBuilder.toString();
    }
}
