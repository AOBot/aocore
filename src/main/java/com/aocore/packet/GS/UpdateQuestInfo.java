/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.QuestInfo;
import com.aocore.d2data.QuestInfoUpdateType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class UpdateQuestInfo extends GSPacket
{
    protected QuestInfo[] quests;
    protected QuestInfoUpdateType type;
    protected int uid;

    public UpdateQuestInfo(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.type = QuestInfoUpdateType.valueOf(data[1]);
        this.uid = byteBuffer.getInt(2);

        this.quests = new QuestInfo[41];
        for (int i=0; i < 41; i++)
            this.quests[i] = new QuestInfo(i, data[6 + i * 2], data[7 + i * 2]);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("UpdateQuestInfo").append("\n");
        stringBuilder.append("quests: ").append(quests).append("\n");
        stringBuilder.append("type: ").append(type).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        return stringBuilder.toString();
    }
}
