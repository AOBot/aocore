/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.GameObjectMode;
import com.aocore.d2data.UnitType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class SetGameObjectMode extends GSPacket
{
    protected UnitType unitType;
    protected int uid;
    protected byte unknown6;
    protected boolean canChangeBack;
    protected GameObjectMode mode;

    public SetGameObjectMode(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.unitType = UnitType.valueOf(data[1]);
        this.uid = byteBuffer.getInt(2);
        this.unknown6 = data[6];
        this.canChangeBack =  data[7] == 1 ? true : false;
        this.mode = GameObjectMode.valueOf(byteBuffer.getInt(8));
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("SetGameObjectMode").append("\n");
        stringBuilder.append("unitType: ").append(unitType).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("unknown6: ").append(unknown6).append("\n");
        stringBuilder.append("canChangeBack: ").append(canChangeBack).append("\n");
        stringBuilder.append("mode: ").append(mode).append("\n");
        return stringBuilder.toString();
    }
}
