/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;

/**
 *
 * @author derek
 */
public class PlayerAttributeNotification extends GSPacket
{
/*
    protected int uid;
    protected StatBase stat;
*/
    public PlayerAttributeNotification(byte... data)
    {
        super(data);
/*
        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.uid = BitConverter.getInt(data, 1);
        BaseStat baseStat = BaseStat.Get(data[5]);
        int val = BitConverter.getInt(data, 6);
        if (baseStat.ValShift > 0)
            val = val >>> baseStat.ValShift;

        if (baseStat.Signed)
            this.stat = new SignedStat(baseStat, val);
        else
            this.stat = new UnsignedStat(baseStat, (int)val);
*/
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("PlayerAttributeNotification").append("\n");
/*
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("stat: ").append(stat).append("\n");
*/
        return stringBuilder.toString();
    }
}
