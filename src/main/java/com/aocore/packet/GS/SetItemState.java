/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.ItemStateType;
import com.aocore.d2data.UnitType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class SetItemState extends GSPacket
{
    protected UnitType ownerType;
    protected int ownerUID;
    protected int itemUID;
    protected byte unknown10;
    protected ItemStateType state;
    protected ItemStateType state2;
    protected byte unknown17;

    public SetItemState(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.ownerType = UnitType.valueOf(data[1]);
        this.ownerUID = byteBuffer.getInt(2);
        this.itemUID = byteBuffer.getInt(6);
        this.unknown10 = data[10];
        this.state = ItemStateType.valueOf(byteBuffer.getInt(11));
        this.state2 = ItemStateType.valueOf(byteBuffer.getShort(15));
        this.unknown17 = data[17];
    }

    public SetItemState(UnitType ownerType, int ownerUID, int itemUID, ItemStateType state)
    {
        data = new byte[] { 0x7D,
            (byte)(ownerType.getId()),
            (byte)(ownerUID), (byte)(ownerUID >>> 8), (byte)(ownerUID >>> 16), (byte)(ownerUID >>> 24),
            (byte)(itemUID), (byte)(itemUID >>> 8), (byte)(itemUID >>> 16), (byte)(itemUID >>> 24),
            0,
            (byte)(state.getId()), (byte)(state.getId() >>> 8), (byte)(state.getId() >>> 16), (byte)(state.getId() >>> 24),
            (byte)(state.getId()), (byte)(state.getId() >>> 8),
            0
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("SetItemState").append("\n");
        stringBuilder.append("ownerType: ").append(ownerType).append("\n");
        stringBuilder.append("ownerUID: ").append(ownerUID).append("\n");
        stringBuilder.append("itemUID: ").append(itemUID).append("\n");
        stringBuilder.append("unknown10: ").append(unknown10).append("\n");
        stringBuilder.append("state: ").append(state).append("\n");
        stringBuilder.append("state2: ").append(state2).append("\n");
        stringBuilder.append("unknown17: ").append(unknown17).append("\n");
        return stringBuilder.toString();
    }
}
