/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.packet.GSPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class PortalOwnership extends GSPacket
{
    protected int ownerUID;
    protected String ownerName;
    protected int portalLocalUID;
    protected int portalRemoteUID;

    public PortalOwnership(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.ownerUID = byteBuffer.getInt(1);
        this.ownerName = ByteConverter.getString(data, 5, 16);
        this.portalLocalUID = byteBuffer.getInt(21);
        this.portalRemoteUID = byteBuffer.getInt(25);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("PortalOwnership").append("\n");
        stringBuilder.append("ownerUID: ").append(ownerUID).append("\n");
        stringBuilder.append("ownerName: ").append(ownerName).append("\n");
        stringBuilder.append("portalLocalUID: ").append(portalLocalUID).append("\n");
        stringBuilder.append("portalRemoteUID: ").append(portalRemoteUID).append("\n");
        return stringBuilder.toString();
    }
}
