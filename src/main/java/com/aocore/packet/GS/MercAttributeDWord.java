/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.packet.GSPacket;

/**
 *
 * @author derek
 */
public class MercAttributeDWord extends GSPacket
{
/*
    protected StatBase stat;
    protected int uid;
*/
    public MercAttributeDWord(byte... data)
    {
        super(data);
/*
        BaseStat baseStat = BaseStat.Get(data[1]);
        this.uid = BitConverter.getInt(data, 2);
        int val = BitConverter.getInt(data, 6);
        if (baseStat.ValShift > 0)
            val = val >>> baseStat.ValShift;
        if (baseStat.Signed)
            this.stat = new SignedStat(baseStat, val);
        else
            this.stat = new UnsignedStat(baseStat, (int)val);
*/
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("MercAttributeDWord").append("\n");
/*
        stringBuilder.append("stat: ").append(stat).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
*/
        return stringBuilder.toString();
    }
}
