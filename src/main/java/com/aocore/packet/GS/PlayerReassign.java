/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.UnitType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class PlayerReassign extends GSPacket
{
    protected UnitType unitType;
    protected int uid;
    protected int x;
    protected int y;
    protected boolean reassign;

    public PlayerReassign(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.unitType = UnitType.valueOf(data[1]);
        this.uid = byteBuffer.getInt(2);
        this.x = byteBuffer.getShort(6);
        this.y = byteBuffer.getShort(8);
        this.reassign = data[10] == 0 ? false : true;
    }

    public PlayerReassign(UnitType type, int uid, int x, int y, boolean reassign)
    {
        data = new byte[] { 0x15,
            (byte)(type.getId()),
            (byte)(uid), (byte)(uid >>> 8), (byte)(uid >>> 16), (byte)(uid >>> 24),
            (byte)(x), (byte)(y >>> 8),
            (byte)(y), (byte)(y >>> 8),
            (byte)(reassign ? 1 : 0)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("PlayerReassign").append("\n");
        stringBuilder.append("unitType: ").append(unitType).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("x: ").append(x).append("\n");
        stringBuilder.append("y: ").append(y).append("\n");
        stringBuilder.append("reassign: ").append(reassign).append("\n");
        return stringBuilder.toString();
    }
}
