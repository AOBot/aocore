/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.CharacterClass;
import com.aocore.d2data.GameObjectID;
import com.aocore.d2data.InformationMessageType;
import com.aocore.d2data.NPCCode;
import com.aocore.d2data.PlayerInformationActionType;
import com.aocore.d2data.PlayerRelationActionType;
import com.aocore.d2data.UnitType;
import com.aocore.packet.GSPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class InformationMessage extends GSPacket
{
    protected InformationMessageType type;
    protected byte actionType;
    protected int objectUID;
    protected String subjectName = null;
    protected String objectName = null;

    protected UnitType slayerType = UnitType.NotApplicable;
    protected CharacterClass charClass = CharacterClass.NotApplicable;
    protected GameObjectID slayerObject = GameObjectID.NotApplicable;
    protected NPCCode slayerMonster = NPCCode.NotApplicable;

    protected PlayerInformationActionType informationType = PlayerInformationActionType.None;
    protected PlayerRelationActionType relationType = PlayerRelationActionType.NotApplicable;

    protected int amount = -1;

    public InformationMessage(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.type = InformationMessageType.valueOf(data[1]);
        this.actionType = data[2];

        switch (this.type)
        {
            case DroppedFromGame:
            case JoinedGame:
            case LeftGame:
                // (BYTE) Action Type = 0x04
                // (DWORD) Unknown (unused)
                // (BYTE) Unknown (unused)
                this.subjectName = ByteConverter.getString(data, 8);	// Character Name
                this.objectName = ByteConverter.getString(data, 24);	// Account Name
                break;

            case NotInGame:
                // (BYTE) Action Type = 0x00
                // (DWORD) Unknown (unused)
                // (BYTE) Unknown (unused)
                this.subjectName = ByteConverter.getString(data, 8);	// Character Name
                break;

            case PlayerSlain:
                // (BYTE) Action Type = 0x04
                this.slayerType = UnitType.valueOf(data[7]);
                this.subjectName = ByteConverter.getString(data, 8);	// Slain Character Name

                if (this.slayerType == UnitType.Player)
                {
                    this.charClass = CharacterClass.valueOf(byteBuffer.getInt(3));
                    this.objectName = ByteConverter.getString(data, 24);
                }
                else if (this.slayerType == UnitType.NPC)
                    this.slayerMonster = NPCCode.valueOf(byteBuffer.getInt(3));
                else if (this.slayerType == UnitType.GameObject)
                    this.slayerObject = GameObjectID.valueOf(byteBuffer.getInt(3));
                break;

            case PlayerRelation:
                this.informationType = PlayerInformationActionType.valueOf(data[2]);
                this.objectUID = byteBuffer.getInt(3);
                this.relationType = PlayerRelationActionType.valueOf(data[7]);
                break;

            case SoJsSoldToMerchants:
                this.amount = byteBuffer.getInt(3);
            break;
        }
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("InformationMessage").append("\n");
        stringBuilder.append("type: ").append(type).append("\n");
        stringBuilder.append("actionType: ").append(actionType).append("\n");
        stringBuilder.append("objectUID: ").append(objectUID).append("\n");
        stringBuilder.append("subjectName: ").append(subjectName).append("\n");
        stringBuilder.append("objectName: ").append(objectName).append("\n");
        stringBuilder.append("slayerType: ").append(slayerType).append("\n");
        stringBuilder.append("charClass: ").append(charClass).append("\n");
        stringBuilder.append("slayerObject: ").append(slayerObject).append("\n");
        stringBuilder.append("slayerMonster: ").append(slayerMonster).append("\n");
        stringBuilder.append("informationType: ").append(informationType).append("\n");
        stringBuilder.append("relationType: ").append(relationType).append("\n");
        stringBuilder.append("amount: ").append(amount).append("\n");
        return stringBuilder.toString();
    }
}
