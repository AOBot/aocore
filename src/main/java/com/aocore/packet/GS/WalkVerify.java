/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.packet.GSPacket;
import com.aocore.util.BitReader;

/**
 *
 * @author derek
 */
public class WalkVerify extends GSPacket
{
    protected int stamina;
    protected int x;
    protected int y;
    protected int state;

    public WalkVerify(byte... data)
    {
        super(data);

        BitReader bitReader = new BitReader(data, 1);
        this.stamina = bitReader.getInt(15);
        this.x = bitReader.getInt(16);
        this.y = bitReader.getInt(16);
        this.state = bitReader.getInt(17);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("WalkVerify").append("\n");
        stringBuilder.append("stamina: ").append(stamina).append("\n");
        stringBuilder.append("x: ").append(x).append("\n");
        stringBuilder.append("y: ").append(y).append("\n");
        stringBuilder.append("state: ").append(state).append("\n");
        return stringBuilder.toString();
    }
}
