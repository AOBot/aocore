/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.SkillType;
import com.aocore.d2data.UnitType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class UnitUseSkill extends GSPacket
{
    protected UnitType unitType;
    protected int uid;
    protected SkillType skill;
    protected int x;
    protected int y;

    public UnitUseSkill(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.unitType = UnitType.valueOf(data[1]);
            this.uid = byteBuffer.getInt(2);
        if (this.unitType == UnitType.GameObject)
            return;
        this.skill = SkillType.valueOf(byteBuffer.getInt(6));
        // 10 Unknown (0x09)
        this.x = byteBuffer.getShort(11);
        this.y = byteBuffer.getShort(13);
        // 15-16 Unknown
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("UnitUseSkill").append("\n");
        stringBuilder.append("unitType: ").append(unitType).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("skill: ").append(skill).append("\n");
        stringBuilder.append("x: ").append(x).append("\n");
        stringBuilder.append("y: ").append(y).append("\n");
        return stringBuilder.toString();
    }
}
