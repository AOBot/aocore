/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.UnitType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class PlayerMove extends GSPacket
{
    protected UnitType unitType;
    protected int uid;
    protected byte movementType;
    protected int targetX;
    protected int targetY;
    protected byte unknown12;
    protected int currentX;
    protected int currentY;

    public PlayerMove(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.unitType = UnitType.valueOf(data[1]);
        this.uid = byteBuffer.getInt(2);
        this.movementType = data[6];        /// TODO: make enum... 0x23=Run, 0x01=Walk, 0x20=knocked back ?
        this.targetX = byteBuffer.getShort(7);
        this.targetY = byteBuffer.getShort(9);
        this.unknown12 = data[12];
        this.currentX = byteBuffer.getShort(12);
        this.currentY = byteBuffer.getShort(14);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("PlayerMove").append("\n");
        stringBuilder.append("unitType: ").append(unitType).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("movementType: ").append(movementType).append("\n");
        stringBuilder.append("targetX: ").append(targetX).append("\n");
        stringBuilder.append("targetY: ").append(targetY).append("\n");
        stringBuilder.append("unknown12: ").append(unknown12).append("\n");
        stringBuilder.append("currentX: ").append(currentX).append("\n");
        stringBuilder.append("currentY: ").append(currentY).append("\n");
        return stringBuilder.toString();
    }
}
