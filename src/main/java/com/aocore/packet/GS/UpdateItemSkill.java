/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.SkillType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class UpdateItemSkill extends GSPacket
{
    protected short unknown1;
    protected int playerUID;
    protected SkillType skill;
    protected int quantity;
    protected short unknown10;

    public UpdateItemSkill(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.unknown1 = byteBuffer.getShort(1);
        this.playerUID = byteBuffer.getInt(3);
        this.skill = SkillType.valueOf(byteBuffer.getShort(7));
        this.quantity = data[9];
        this.unknown10 = byteBuffer.getShort(10);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("UpdateItemSkill").append("\n");
        stringBuilder.append("unknown1: ").append(unknown1).append("\n");
        stringBuilder.append("playerUID: ").append(playerUID).append("\n");
        stringBuilder.append("skill: ").append(skill).append("\n");
        stringBuilder.append("quantity: ").append(quantity).append("\n");
        stringBuilder.append("unknown10: ").append(unknown10).append("\n");
        return stringBuilder.toString();
    }
}
