/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.SummonActionType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class SummonAction extends GSPacket
{
    protected SummonActionType actionType;
    protected byte skillTree;
    protected int petType;
    protected int playerUID;
    protected int petUID;

    public SummonAction(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.actionType = SummonActionType.valueOf(data[1]);
        this.skillTree = data[2];
        this.petType = byteBuffer.getShort(3);
        this.playerUID = byteBuffer.getInt(5);
        this.petUID = byteBuffer.getInt(9);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("SummonAction").append("\n");
        stringBuilder.append("actionType: ").append(actionType).append("\n");
        stringBuilder.append("skillTree: ").append(skillTree).append("\n");
        stringBuilder.append("petType: ").append(petType).append("\n");
        stringBuilder.append("playerUID: ").append(playerUID).append("\n");
        stringBuilder.append("petUID: ").append(petUID).append("\n");
        return stringBuilder.toString();
    }
}
