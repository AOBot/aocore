/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.packet.GSPacket;
import com.aocore.util.BitReader;
import java.util.List;

/**
 *
 * @author derek
 */
public class UpdateItemStats extends GSPacket
{
/*
    protected int unknown8b;
    protected int uid;
    protected List<StatBase> stats = new List<StatBase>();

    protected int unknown60b;
    protected int unknown61b = -1;
    protected int unknown78b = -1;

    protected long offset;
    protected byte[] unknownEnd;
*/
    public UpdateItemStats(byte... data)
    {
        super(data);
/*
        BitReader bitReader = new BitReader(data, 1);
        this.unknown8b = bitReader.getInt(10);
        this.uid = bitReader.getInt();

        while (bitReader.getBoolean(1))
        {
            BaseStat baseStat = BaseStat.Get(bitReader.getInt(9));
            this.unknown60b = bitReader.getInt(1);

            switch (baseStat.Type)
            {
                case StatType.ChargedSkill:
                    this.unknown61b = br.ReadInt32(1);

                    int currentCharges = br.ReadInt32(8);
                    int maxCharges = br.ReadInt32(8);

                    this.unknown78b = br.ReadInt32(1);

                    int level = br.ReadInt32(6);
                    int skill = br.ReadInt32(10);

                    this.stats.Add(new ChargedSkillStat(baseStat,
                        level,
                        skill,
                        currentCharges,
                        maxCharges
                    ));
                    break;
                default:
                    if (baseStat.Signed)
                        this.stats.Add(new SignedStat(baseStat, br.ReadInt32(baseStat.SendBits)));
                    else
                        this.stats.Add(new UnsignedStat(baseStat, br.Readint32(baseStat.SendBits)));
                    break;
            }
        }
        this.offset = br.Position;
        this.unknownEnd = br.ReadByteArray();
*/
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("UpdateItemStats").append("\n");
/*
        stringBuilder.append("unknown8b: ").append(unknown8b).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("stats: ").append(stats).append("\n");
        stringBuilder.append("unknown60b: ").append(unknown60b).append("\n");
        stringBuilder.append("unknown61b: ").append(unknown61b).append("\n");
        stringBuilder.append("unknown78b: ").append(unknown78b).append("\n");
        stringBuilder.append("offset: ").append(offset).append("\n");
        stringBuilder.append("unknownEnd: ").append(unknownEnd).append("\n");
*/
        return stringBuilder.toString();
    }
}
