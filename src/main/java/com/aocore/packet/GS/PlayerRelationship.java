/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.PlayerRelationshipType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class PlayerRelationship extends GSPacket
{
    protected int subjectUID;
    protected int objectUID;
    protected PlayerRelationshipType relations;

    public PlayerRelationship(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.subjectUID = byteBuffer.getInt(1);
        this.objectUID = byteBuffer.getInt(5);
        this.relations = PlayerRelationshipType.valueOf(byteBuffer.getShort(9));
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("PlayerRelationship").append("\n");
        stringBuilder.append("subjectUID: ").append(subjectUID).append("\n");
        stringBuilder.append("objectUID: ").append(objectUID).append("\n");
        stringBuilder.append("relations: ").append(relations).append("\n");
        return stringBuilder.toString();
    }
}
