/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.NPCMode;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class SetNPCMode extends GSPacket
{
    protected int uid;
    protected NPCMode mode;
    protected int x;
    protected int y;
    protected byte life;
    protected byte unknown11;

    public SetNPCMode(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.uid = byteBuffer.getInt(1);
        this.mode = NPCMode.valueOf(data[5]);
        this.x = byteBuffer.getShort(6);
        this.y = byteBuffer.getShort(8);
        this.life = data[10];
        this.unknown11 = data[11];
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("SetNPCMode").append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("mode: ").append(mode).append("\n");
        stringBuilder.append("x: ").append(x).append("\n");
        stringBuilder.append("y: ").append(y).append("\n");
        stringBuilder.append("life: ").append(life).append("\n");
        stringBuilder.append("unknown11: ").append(unknown11).append("\n");
        return stringBuilder.toString();
    }
}
