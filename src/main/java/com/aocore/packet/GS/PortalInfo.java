/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.AreaLevel;
import com.aocore.d2data.TownPortalState;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class PortalInfo extends GSPacket
{
    protected TownPortalState state;
    protected AreaLevel destination;
    protected int uid;

    public PortalInfo(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.state = TownPortalState.valueOf(data[1]);
        this.destination = AreaLevel.valueOf(data[2]);
        this.uid = byteBuffer.getInt( 3);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("PortalInfo").append("\n");
        stringBuilder.append("state: ").append(state).append("\n");
        stringBuilder.append("destination: ").append(destination).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        return stringBuilder.toString();
    }
}
