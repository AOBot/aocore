/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class Relator1 extends GSPacket
{
    protected short param1;
    protected int uid;
    protected int param2;

    public Relator1(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.param1 = byteBuffer.getShort(1);
        this.uid = byteBuffer.getInt(3);
        this.param2 = byteBuffer.getInt(7);
    }

    public Relator1(int uid, short param1, int param2)
    {
        data = new byte[] { 0x47,
            (byte)(param1), (byte)(param1 >>> 8),
            (byte)(uid), (byte)(uid >>> 8), (byte)(uid >>> 16), (byte)(uid >>> 24),
            (byte)(param2), (byte)(param2 >>> 8), (byte)(param2 >>> 16), (byte)(param2 >>> 24)
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("Relator1").append("\n");
        stringBuilder.append("param1: ").append(param1).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("param2: ").append(param2).append("\n");
        return stringBuilder.toString();
    }
}
