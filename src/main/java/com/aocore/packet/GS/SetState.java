/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.UnitType;
import com.aocore.packet.GSPacket;
import com.aocore.util.BitReader;
import java.nio.ByteBuffer;
import java.util.List;

/**
 *
 * @author derek
 */
public class SetState extends GSPacket
{
/*
    protected UnitType unitType;
    protected int uid;
    protected BaseState state;
    protected List<StatBase> stats;
    protected byte[] unknownEnd;
*/
    public SetState(byte... data)
    {
        super(data);
/*
        ByteBuffer byteBuffer = ByteBuffer.wrap(data);         byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.unitType = (UnitType)data[1];
        this.uid = BitConverter.getInt(data, 2);
        // (byte) Length
        this.state = BaseState.Get(data[7]);
        this.stats = new List<StatBase>();

        BitReader br = new BitReader(data, 8);
        int statID, val, param;
        BaseStat stat;
        while (true)
        {
            statID = br.ReadInt32(9);
            if (statID == 0x1FF)
                break;

            stat = BaseStat.Get(statID);
            val = br.ReadInt32(stat.SendBits);
            if (stat.SendParamBits > 0)
            {
                param = br.ReadInt32(stat.SendParamBits);
                if (stat.Signed)
                    this.stats.Add(new SignedStatParam(stat, val, param));
                else
                    this.stats.Add(new UnsignedStatParam(stat, (int)val, (int)param));
            }
            else
            {
                if (stat.Signed)
                    this.stats.Add(new SignedStat(stat, val));
                else
                    this.stats.Add(new UnsignedStat(stat, (int)val));
            }
        }
        this.unknownEnd = br.ReadByteArray();
*/
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("SetState").append("\n");
/*
        stringBuilder.append("unitType: ").append(unitType).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("state: ").append(state).append("\n");
        stringBuilder.append("stats: ").append(stats).append("\n");
        stringBuilder.append("unknownEnd: ").append(unknownEnd).append("\n");
*/
        return stringBuilder.toString();
    }
}
