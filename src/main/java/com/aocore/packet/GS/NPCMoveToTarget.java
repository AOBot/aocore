/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.UnitType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class NPCMoveToTarget extends GSPacket
{
    protected int uid;
    protected byte movementType;
    protected int currentX;
    protected int currentY;
    protected UnitType targetType;
    protected int targetUID;

    public NPCMoveToTarget(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.uid = byteBuffer.getInt(1);
        this.movementType = data[5];
        this.currentX = byteBuffer.getShort(6);
        this.currentY = byteBuffer.getShort(8);
        this.targetType = UnitType.valueOf(data[10]);
        this.targetUID = byteBuffer.getInt(11);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("NPCMoveToTarget").append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("movementType: ").append(movementType).append("\n");
        stringBuilder.append("currentX: ").append(currentX).append("\n");
        stringBuilder.append("currentY: ").append(currentY).append("\n");
        stringBuilder.append("targetType: ").append(targetType).append("\n");
        stringBuilder.append("targetUID: ").append(targetUID).append("\n");
        return stringBuilder.toString();
    }
}
