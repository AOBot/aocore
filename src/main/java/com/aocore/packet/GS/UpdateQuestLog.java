/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.QuestLog;
import com.aocore.packet.GSPacket;

/**
 *
 * @author derek
 */
public class UpdateQuestLog extends GSPacket
{
    protected QuestLog[] quests;

    public UpdateQuestLog(byte... data)
    {
        super(data);

        this.quests = new QuestLog[41];
        for (int i = 0; i < 41; i++)
            this.quests[i] = new QuestLog(i, data[i + 1]);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("UpdateQuestLog").append("\n");
        stringBuilder.append("quests: ").append(quests).append("\n");
        return stringBuilder.toString();
    }
}
