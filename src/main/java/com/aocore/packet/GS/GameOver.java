/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.awesomo.api.events.GameOverEvent;
import com.aocore.packet.GSPacket;
import com.aocore.proxy.Proxy;

/**
 *
 * @author derek
 */
public class GameOver extends GSPacket
{
    public GameOver(byte... data)
    {
        super(data);
    }

    public GameOver()
    {
        data = new byte[] { (byte)0xB0 };
    }

    @Override
    public void process(Proxy proxy)
    {
        proxy.getAwesomO().fireEvents(GameOverEvent.class);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("GameOver").append("\n");
        return stringBuilder.toString();
    }
}
