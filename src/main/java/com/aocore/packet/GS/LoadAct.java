/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.ActLocation;
import com.aocore.d2data.AreaLevel;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class LoadAct extends GSPacket
{
    protected ActLocation act;
    protected int mapId;
    protected AreaLevel townArea;

    public LoadAct(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.act = ActLocation.valueOf(data[1]);
        this.mapId = byteBuffer.getInt(2);
        this.townArea = AreaLevel.valueOf(byteBuffer.getShort(6));
    }

    public ActLocation getAct()
    {
        return act;
    }

    public int getMapId()
    {
        return mapId;
    }

    public AreaLevel getTownArea()
    {
        return townArea;
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("LoadAct").append("\n");
        stringBuilder.append("act: ").append(act).append("\n");
        stringBuilder.append("mapId: ").append(mapId).append("\n");
        stringBuilder.append("townArea: ").append(townArea).append("\n");
        return stringBuilder.toString();
    }
}
