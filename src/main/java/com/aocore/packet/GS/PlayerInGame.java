/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.CharacterClass;
import com.aocore.packet.GSPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class PlayerInGame extends GSPacket
{
    protected String name;
    protected CharacterClass charClass;
    protected short level;
    protected short partyID;
    protected int uid;

    public PlayerInGame(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        // [1-2] short length
        this.uid = byteBuffer.getInt(3);
        this.charClass = CharacterClass.valueOf(data[7]);
        this.name = ByteConverter.getString(data, 8);
        this.level = byteBuffer.getShort(24);
        this.partyID = byteBuffer.getShort(26);
        // [28+] 8 bytes null padding... used to be account maybe?
    }

    public static byte[] Build(int uid, CharacterClass charClass, String name, short level, short partyID)
    {
        if (name == null || name.length() == 0 || name.length() > 16)
            throw new IllegalArgumentException("name");

        byte[] bytes = new byte[34];
        bytes[0] = 0x5B;
        bytes[1] = 34;
        bytes[3] = (byte)(uid);
        bytes[4] = (byte)(uid >>> 8);
        bytes[5] = (byte)(uid >>> 16);
        bytes[6] = (byte)(uid >>> 24);
        bytes[24] = (byte)(level);
        bytes[25] = (byte)(level >>> 8);
        bytes[26] = (byte)(partyID);
        bytes[27] = (byte)(partyID >>> 8);
        for (int i = 0; i < name.length(); i++)
            bytes[8 + i] = (byte)name.getBytes()[i];

        return bytes;
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("PlayerInGame").append("\n");
        stringBuilder.append("name: ").append(name).append("\n");
        stringBuilder.append("charClass: ").append(charClass).append("\n");
        stringBuilder.append("level: ").append(level).append("\n");
        stringBuilder.append("partyID: ").append(partyID).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        return stringBuilder.toString();
    }
}
