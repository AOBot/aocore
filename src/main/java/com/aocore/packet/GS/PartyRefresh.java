/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class PartyRefresh extends GSPacket
{
    protected int slotNumber;
    protected byte alternator;
    protected int count;

    public PartyRefresh(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.slotNumber = byteBuffer.getInt(1);
        this.alternator = data[5];
        this.count = byteBuffer.getInt(4);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("PartyRefresh").append("\n");
        stringBuilder.append("slotNumber: ").append(slotNumber).append("\n");
        stringBuilder.append("alternator: ").append(alternator).append("\n");
        stringBuilder.append("count: ").append(count).append("\n");
        return stringBuilder.toString();
    }
}
