/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.AreaLevel;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class PartyMemberUpdate extends GSPacket
{
    protected boolean isPlayer;
    protected int lifePercent;
    protected int uid;
    protected AreaLevel area;

    public PartyMemberUpdate(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.isPlayer = data[1] == 1 ? true : false;
        this.lifePercent = byteBuffer.getShort(2);
        this.uid = byteBuffer.getInt(4);
        this.area = AreaLevel.valueOf(byteBuffer.getShort(8));
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("PartyMemberUpdate").append("\n");
        stringBuilder.append("isPlayer: ").append(isPlayer).append("\n");
        stringBuilder.append("lifePercent: ").append(lifePercent).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("area: ").append(area).append("\n");
        return stringBuilder.toString();
    }
}
