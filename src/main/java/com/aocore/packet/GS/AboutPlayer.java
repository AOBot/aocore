/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.PlayerRelationshipType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class AboutPlayer extends GSPacket
{
    protected int uid;
    protected short partyID;
    protected int level;
    protected PlayerRelationshipType relationship;
    protected boolean isInMyParty;
    protected byte unknown12;

    public AboutPlayer(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.uid = byteBuffer.getInt(1);
        this.partyID = byteBuffer.getShort(5);
        this.level = byteBuffer.getShort(7);
        this.relationship = PlayerRelationshipType.valueOf(byteBuffer.getShort(9));
        this.isInMyParty = data[11] == 0 ? false : true;
        this.unknown12 = data[12];
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("AboutPlayer").append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("partyID: ").append(partyID).append("\n");
        stringBuilder.append("level: ").append(level).append("\n");
        stringBuilder.append("relationship: ").append(relationship).append("\n");
        stringBuilder.append("isInMyParty: ").append(isInMyParty).append("\n");
        stringBuilder.append("unknown12: ").append(unknown12).append("\n");
        return stringBuilder.toString();
    }
}
