/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.ItemUIAction;
import com.aocore.packet.GSPacket;

/**
 *
 * @author derek
 */
public class UpdateItemUI extends GSPacket
{
    protected ItemUIAction action;

    public UpdateItemUI(byte... data)
    {
        super(data);

        this.action = ItemUIAction.valueOf(data[1]);
    }

    public UpdateItemUI(ItemUIAction action)
    {
        data = new byte[] { 0x77, (byte)(action.getId()) };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("UpdateItemUI").append("\n");
        stringBuilder.append("action: ").append(action).append("\n");
        return stringBuilder.toString();
    }
}
