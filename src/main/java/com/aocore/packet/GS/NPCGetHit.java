/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.UnitType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class NPCGetHit extends GSPacket
{
    protected UnitType unitType;
    protected int uid;
    protected int animation;
    protected byte life;

    public NPCGetHit(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.unitType = UnitType.valueOf(data[1]);
        this.uid = byteBuffer.getInt(2);
        this.animation = byteBuffer.getShort(6);
        this.life = data[8];
    }

    public NPCGetHit(UnitType type, int uid, byte life, int anim)
    {
        if (life > 128)
            throw new IllegalArgumentException("life");

        data = new byte[] { 0x0C,
            (byte)(type.getId()),
            (byte)(uid), (byte)(uid >>> 8), (byte)(uid >>> 16), (byte)(uid >>> 24),
            (byte)(anim), (byte)(anim >>> 8),
            life
        };
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("NPCGetHit").append("\n");
        stringBuilder.append("unitType: ").append(unitType).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("animation: ").append(animation).append("\n");
        stringBuilder.append("life: ").append(life).append("\n");
        return stringBuilder.toString();
    }
}
