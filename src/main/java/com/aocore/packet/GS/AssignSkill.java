/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.SkillHand;
import com.aocore.d2data.SkillType;
import com.aocore.d2data.UnitType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class AssignSkill extends GSPacket
{
    protected UnitType unitType;
    protected int uid;
    protected SkillHand hand;
    protected SkillType skill;
    protected int chargedItemUID;

    public AssignSkill(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.unitType = UnitType.valueOf(data[1]);
        this.uid = byteBuffer.getInt(2);
        this.hand = SkillHand.valueOf(data[6]);
        this.skill = SkillType.valueOf(byteBuffer.getShort(7));
        this.chargedItemUID = byteBuffer.getInt(9);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("AssignSkill").append("\n");
        stringBuilder.append("unitType: ").append(unitType).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("hand: ").append(hand).append("\n");
        stringBuilder.append("skill: ").append(skill).append("\n");
        stringBuilder.append("chargedItemUID: ").append(chargedItemUID).append("\n");
        return stringBuilder.toString();
    }
}
