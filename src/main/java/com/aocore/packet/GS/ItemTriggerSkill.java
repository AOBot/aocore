/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.ItemEventCause;
import com.aocore.d2data.SkillType;
import com.aocore.d2data.UnitType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class ItemTriggerSkill extends GSPacket
{
    protected UnitType ownerType;
    protected int ownerUID;
    protected SkillType skill;
    protected byte level;
    protected UnitType targetType;
    protected int targetUID;
    protected ItemEventCause cause;

    public ItemTriggerSkill(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.ownerType = UnitType.valueOf(data[1]);
        this.ownerUID = byteBuffer.getInt(2);
        this.skill = SkillType.valueOf(byteBuffer.getShort(6));
        this.level = data[8];
        this.targetType = UnitType.valueOf(data[9]);
        this.targetUID = byteBuffer.getInt(10);
        this.cause = ItemEventCause.valueOf(byteBuffer.getShort(14));
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("ItemTriggerSkill").append("\n");
        stringBuilder.append("ownerType: ").append(ownerType).append("\n");
        stringBuilder.append("ownerUID: ").append(ownerUID).append("\n");
        stringBuilder.append("skill: ").append(skill).append("\n");
        stringBuilder.append("level: ").append(level).append("\n");
        stringBuilder.append("targetType: ").append(targetType).append("\n");
        stringBuilder.append("targetUID: ").append(targetUID).append("\n");
        stringBuilder.append("cause: ").append(cause).append("\n");
        return stringBuilder.toString();
    }
}
