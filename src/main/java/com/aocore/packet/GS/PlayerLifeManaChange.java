/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.packet.GSPacket;
import com.aocore.util.BitReader;

/**
 *
 * @author derek
 */
public class PlayerLifeManaChange extends GSPacket
{
    protected int life;
    protected int mana;
    protected int stamina;
    protected int x;
    protected int y;

    public PlayerLifeManaChange(byte... data)
    {
        super(data);
        /*
        this.life = BitHelper.ReadInt32(15, data, 1, 0);
        this.mana = BitHelper.ReadInt32(15, data, 2, 7);
        this.stamina = BitHelper.ReadInt32(15, data, 4, 6);
        this.x = BitHelper.ReadInt32(16, data, 6, 5);
        this.y = BitHelper.ReadInt32(16, data, 8, 5);
         */
        BitReader bitReader = new BitReader(data, 1);
        this.life = bitReader.getInt(15);
        this.mana = bitReader.getInt(15);
        this.stamina = bitReader.getInt(15);
        this.x = bitReader.getInt(16);
        this.y = bitReader.getInt(16);
    }

    public int getLife()
    {
        return life;
    }

    public int getMana()
    {
        return mana;
    }

    public int getStamina()
    {
        return stamina;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("PlayerLifeManaChange").append("\n");
        stringBuilder.append("life: ").append(life).append("\n");
        stringBuilder.append("mana: ").append(mana).append("\n");
        stringBuilder.append("stamina: ").append(stamina).append("\n");
        stringBuilder.append("x: ").append(x).append("\n");
        stringBuilder.append("y: ").append(y).append("\n");
        return stringBuilder.toString();
    }
}
