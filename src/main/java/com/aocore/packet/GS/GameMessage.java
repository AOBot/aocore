/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.GameMessageType;
import com.aocore.d2data.UnitType;
import com.aocore.packet.GSPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class GameMessage extends GSPacket
{
    protected GameMessageType messageType;
    protected String message;
    // Game message fields
    protected String playerName = null;
    // Overhead fields
    protected UnitType unitType = UnitType.NotApplicable;
    protected int uid = 0;
    protected int random = -1;

    public GameMessage(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.messageType = GameMessageType.valueOf(byteBuffer.getShort(1));
        if (this.messageType == GameMessageType.OverheadMessage)
        {
            this.unitType = UnitType.valueOf(data[3]);
            this.uid = byteBuffer.getInt(4);
            this.random = byteBuffer.getShort(8);
            //(NULLSTRING) Unused (00)
            this.message = ByteConverter.getString(data, 11);
        }
        else
        {
            //(BYTE) Unknown (02)
            //(DWORD) Unknown (null - unused ?)
            //(BYTE) Unknown (00)
            //(BYTE) Unknown (0x59 on chat, 0x35 on receive whisper ??)
            this.playerName = ByteConverter.getString(data, 10);
            this.message = ByteConverter.getString(data, 11 + this.playerName.length());
        }
    }

    public GameMessage(String name, String message)
    {
        data = new byte[name.length() + message.length() + 12];
        int offset = 0;

        data[offset++] = 0x26;
        data[offset++] = 0x01;
        data[offset++] = 0x00;
        data[offset++] = 0x02;
        data[offset++] = 0x00;
        data[offset++] = 0x00;
        data[offset++] = 0x00;
        data[offset++] = 0x00;
        data[offset++] = 0x00;
        data[offset++] = 0x05;

        for (int i = 0; i < name.length(); i++)
        {
            data[offset++] = name.getBytes()[i];
        }
        data[offset++] = 0x00;

        for (int i = 0; i < message.length(); i++)
        {
            data[offset++] = message.getBytes()[i];
        }
        data[offset++] = 0x00;
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("GameMessage").append("\n");
        stringBuilder.append("messageType: ").append(messageType).append("\n");
        stringBuilder.append("message: ").append(message).append("\n");
        stringBuilder.append("playerName: ").append(playerName).append("\n");
        stringBuilder.append("unitType: ").append(unitType).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("random: ").append(random).append("\n");
        return stringBuilder.toString();
    }
}
