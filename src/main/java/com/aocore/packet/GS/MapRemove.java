/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.AreaLevel;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class MapRemove extends GSPacket
{
    protected int x;
    protected int y;
    protected AreaLevel area;

    public MapRemove(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.x = byteBuffer.getShort(1);
        this.y = byteBuffer.getShort(3);
        this.area = AreaLevel.valueOf(data[5]);
    }

    public MapRemove(int x, int y, AreaLevel area)
    {
        data = new byte[] { 0x08,
                (byte)(x), (byte)(x >>> 8),
                (byte)(y), (byte)(y >>> 8),
                (byte)area.getId()};
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public AreaLevel getArea()
    {
        return area;
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("MapRemove").append("\n");
        stringBuilder.append("x: ").append(x).append("\n");
        stringBuilder.append("y: ").append(y).append("\n");
        stringBuilder.append("area: ").append(area).append("\n");
        return stringBuilder.toString();
    }
}
