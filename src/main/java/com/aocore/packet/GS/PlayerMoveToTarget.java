/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.UnitType;
import com.aocore.packet.GSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class PlayerMoveToTarget extends GSPacket
{
    protected UnitType unitType;
    protected int uid;
    protected byte movementType;
    protected UnitType targetType;
    protected int targetUID;
    protected int currentX;
    protected int currentY;

    public PlayerMoveToTarget(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN); 
        this.unitType = UnitType.valueOf(data[1]);
        this.uid = byteBuffer.getInt(2);
        //TODO: make enum (flags ? value here is above & 1 !?!? ... 0x24=Run, 0x00=Walk
        this.movementType = data[6];
        this.targetType = UnitType.valueOf(data[7]);
        this.targetUID = byteBuffer.getInt(8);
        this.currentX = byteBuffer.getShort(12);
        this.currentY = byteBuffer.getShort(14);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("PlayerMoveToTarget").append("\n");
        stringBuilder.append("unitType: ").append(unitType).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("movementType: ").append(movementType).append("\n");
        stringBuilder.append("targetType: ").append(targetType).append("\n");
        stringBuilder.append("targetUID: ").append(targetUID).append("\n");
        stringBuilder.append("currentX: ").append(currentX).append("\n");
        stringBuilder.append("currentY: ").append(currentY).append("\n");
        return stringBuilder.toString();
    }
}
