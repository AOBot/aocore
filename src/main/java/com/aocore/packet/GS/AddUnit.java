/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.GS;

import com.aocore.d2data.UnitType;
import com.aocore.packet.GSPacket;
import com.aocore.util.BitReader;
import java.nio.ByteBuffer;
import java.util.List;

/**
 *
 * @author derek
 */
public class AddUnit extends GSPacket
{
/*
    protected UnitType unitType;
    protected int uid;
    protected List<NPCState> states;
    protected long offset;
    protected byte[] unknownEnd;
*/
    public AddUnit(byte... data)
    {
        super(data);
/*
        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.unitType = UnitType.valueOf(data[1]);
        this.uid = byteBuffer.getInt(2);
        //data[6] = packet length

        this.states = new List<NPCState>();

        BitReader br = new BitReader(data, 7);
        int stateID, statID;
        int val, param;
        while (true)
        {
            stateID = br.ReadInt32(8);
            if (stateID == 0xFF)
                break;

            if (br.ReadBoolean(1))
            {
                List<StatBase> stats = new List<StatBase>();
                while (true)
                {
                    statID = br.ReadInt32(9);
                    if (statID == 0x1FF)
                        break;

                    BaseStat baseStat = BaseStat.Get(statID);
                    val = br.ReadInt32(baseStat.SendBits);
                    if (baseStat.SendParamBits > 0)
                    {
                        param = br.ReadInt32(baseStat.SendParamBits);
                        if (baseStat.Signed)
                            stats.Add(new SignedStatParam(baseStat, val, param));
                        else
                            stats.Add(new UnsignedStatParam(baseStat, (int)val, (int)param));
                    }
                    else
                    {
                        if (baseStat.Signed)
                            stats.Add(new SignedStat(baseStat, val));
                        else
                            stats.Add(new UnsignedStat(baseStat, (int)val));
                    }
                }
                this.states.Add(new NPCState(BaseState.Get(stateID), stats));
            }
            else
                this.states.Add(new NPCState(BaseState.Get(stateID)));
        }

        this.offset = br.Position;
        this.unknownEnd = br.ReadByteArray();
*/
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("GSPacket: ").append("AddUnit").append("\n");
/*
        stringBuilder.append("unitType: ").append(unitType).append("\n");
        stringBuilder.append("uid: ").append(uid).append("\n");
        stringBuilder.append("states: ").append(states).append("\n");
        stringBuilder.append("offset: ").append(offset).append("\n");
        stringBuilder.append("unknownEnd: ").append(unknownEnd).append("\n");
        stringBuilder.append("unknown12: ").append(unknown12).append("\n");
*/
        return stringBuilder.toString();
    }
}
