/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.RC;

import com.aocore.d2data.CharacterClass;
import com.aocore.d2data.CharacterFlags;
import com.aocore.packet.RCPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class CharacterCreationRequest extends RCPacket
{
    protected CharacterClass charClass;
    protected CharacterFlags flags;
    protected String name;

    public CharacterCreationRequest(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.charClass = CharacterClass.valueOf(byteBuffer.getInt(3));
        this.flags = CharacterFlags.valueOf(byteBuffer.getShort(7));
        this.name = ByteConverter.getString(data, 9);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RCPacket: ").append("CharacterCreationRequest").append("\n");
        stringBuilder.append("charClass: ").append(charClass).append("\n");
        stringBuilder.append("flags: ").append(flags).append("\n");
        stringBuilder.append("name: ").append(name).append("\n");
        return stringBuilder.toString();
    }
}
