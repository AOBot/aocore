/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.RC;

import com.aocore.d2data.GameDifficulty;
import com.aocore.packet.RCPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class CreateGameRequest extends RCPacket
{
    protected short requestID;
    protected byte unknown1;
    protected GameDifficulty difficulty;
    protected short unknown2;
    protected byte unknown3;
    protected byte levelRestriction;
    protected byte maxPlayers;
    protected String name;
    protected String password = null;
    protected String description = null;

    public CreateGameRequest(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.requestID = byteBuffer.getShort(3);
        this.unknown1 = data[5];
        this.difficulty = GameDifficulty.valueOf((data[6] >>> 4));
        this.unknown2 = byteBuffer.getShort(7);
        this.unknown3 = data[9];
        this.levelRestriction = data[10];
        this.maxPlayers = data[11];
        this.name = ByteConverter.getString(data, 12);
        if (data.length > 15 + this.name.length())
            this.password = ByteConverter.getString(data, 14 + this.name.length());
        if (data.length > 15 + this.name.length() + (this.password == null ? 0 : this.password.length()))
            this.description = ByteConverter.getString(data, 14 + this.name.length() + (this.password == null ? 0 : this.password.length()));
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RCPacket: ").append("CreateGameRequest").append("\n");
        stringBuilder.append("requestID: ").append(requestID).append("\n");
        stringBuilder.append("unknown1: ").append(unknown1).append("\n");
        stringBuilder.append("difficulty: ").append(difficulty).append("\n");
        stringBuilder.append("unknown2: ").append(unknown2).append("\n");
        stringBuilder.append("unknown3: ").append(unknown3).append("\n");
        stringBuilder.append("levelRestriction: ").append(levelRestriction).append("\n");
        stringBuilder.append("maxPlayers: ").append(maxPlayers).append("\n");
        stringBuilder.append("name: ").append(name).append("\n");
        stringBuilder.append("password: ").append(password).append("\n");
        stringBuilder.append("description: ").append(description).append("\n");
        return stringBuilder.toString();
    }
}
