/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.RC;

import com.aocore.packet.RCPacket;
import com.aocore.proxy.Proxy;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 *
 * @author derek
 */
public class RealmStartupRequest extends RCPacket
{
    protected int cookie;
    protected String username;

    public RealmStartupRequest(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.cookie = byteBuffer.getInt(3);
        // 15 * DWORD = MCP Chunk DWORDS 2 - 16
        this.username = ByteConverter.getString(data, 67);
    }

    @Override
    public void process(Proxy proxy)
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byte[] hash = new byte[64];

        byteBuffer.position(3);
        byteBuffer.get(hash, 0, 64);

        try
        {
            proxy.setAwesomO(Proxy.ttt.get(Arrays.hashCode(hash)));
            proxy.connectClient(proxy.getAwesomO().getRealmAddress());
        }
        catch (Exception e)
        {
            proxy.stopProxy();
        }
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RCPacket: ").append("RealmStartupRequest").append("\n");
        stringBuilder.append("cookie: ").append(cookie).append("\n");
        stringBuilder.append("username: ").append(username).append("\n");
        return stringBuilder.toString();
    }
}
