/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CC;

import com.aocore.packet.CCPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class RealmLogonRequest extends CCPacket
{
    protected int cookie;
    protected String realm;

    public RealmLogonRequest(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.cookie = byteBuffer.getInt(4);
        //TODO: 5 * DWORD = hashed password
        this.realm = ByteConverter.getString(data, 28);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CCPacket: ").append("RealmLogonRequest").append("\n");
        stringBuilder.append("cookie: ").append(cookie).append("\n");
        stringBuilder.append("realm: ").append(realm).append("\n");
        return stringBuilder.toString();
    }
}
