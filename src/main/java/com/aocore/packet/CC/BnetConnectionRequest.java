/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CC;

import com.aocore.d2data.BattleNetClient;
import com.aocore.d2data.BattleNetPlatform;
import com.aocore.packet.CCPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class BnetConnectionRequest extends CCPacket
{
    protected int protocol;
    protected BattleNetPlatform platform;
    protected BattleNetClient client;
    protected int version;
    protected int language;
    protected int localIP;
    protected int timeZoneBias;
    protected int localeID;
    protected int languageID;
    protected String countryAbbreviation;
    protected String countryName;

    public BnetConnectionRequest(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.protocol = byteBuffer.getInt(4);
        this.platform = BattleNetPlatform.valueOf(byteBuffer.getInt(8));
        this.client = BattleNetClient.valueOf(byteBuffer.getInt(12));
        this.version = byteBuffer.getInt(16);
        this.language = byteBuffer.getInt(20);
        this.localIP = byteBuffer.getInt(24);
        this.timeZoneBias = byteBuffer.getInt(28);
        this.localeID = byteBuffer.getInt(32);
        this.languageID = byteBuffer.getInt(36);
        this.countryAbbreviation = ByteConverter.getString(data, 40);
        this.countryName = ByteConverter.getString(data, 41 + this.countryAbbreviation.length());
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CCPacket: ").append("BnetConnectionRequest").append("\n");
        stringBuilder.append("protocol: ").append(protocol).append("\n");
        stringBuilder.append("platform: ").append(platform).append("\n");
        stringBuilder.append("client: ").append(client).append("\n");
        stringBuilder.append("version: ").append(version).append("\n");
        stringBuilder.append("language: ").append(language).append("\n");
        stringBuilder.append("localIP: ").append(localIP).append("\n");
        stringBuilder.append("timeZoneBias: ").append(timeZoneBias).append("\n");
        stringBuilder.append("localeID: ").append(localeID).append("\n");
        stringBuilder.append("languageID: ").append(languageID).append("\n");
        stringBuilder.append("countryAbbreviation: ").append(countryAbbreviation).append("\n");
        stringBuilder.append("countryName: ").append(countryName).append("\n");
        return stringBuilder.toString();
    }
}
