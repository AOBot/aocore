/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CC;

import com.aocore.d2data.BattleNetClient;
import com.aocore.d2data.BattleNetPlatform;
import com.aocore.packet.CCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;

/**
 *
 * @author derek
 */
public class AdInfoRequest extends CCPacket
{
    protected BattleNetPlatform platform;
    protected BattleNetClient client;
    protected int id;
    protected Date timestamp;

    public AdInfoRequest(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.platform = BattleNetPlatform.valueOf(byteBuffer.getInt(4));
        this.client = BattleNetClient.valueOf(byteBuffer.getInt(8));
        this.id = byteBuffer.getInt(12);
        this.timestamp = new Date(byteBuffer.getInt(16));
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CCPacket: ").append("AdInfoRequest").append("\n");
        stringBuilder.append("platform: ").append(platform).append("\n");
        stringBuilder.append("client: ").append(client).append("\n");
        stringBuilder.append("id: ").append(id).append("\n");
        stringBuilder.append("timestamp: ").append(timestamp).append("\n");
        return stringBuilder.toString();
    }
}
