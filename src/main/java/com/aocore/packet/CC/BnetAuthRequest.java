/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CC;

import com.aocore.cdkey.CdKey;
import com.aocore.d2data.CDKeyInfo;
import com.aocore.packet.CCPacket;
import com.aocore.proxy.ChatProxy;
import com.aocore.proxy.Proxy;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class BnetAuthRequest extends CCPacket
{
    protected int clientToken;
    protected int gameVersion;
    protected int gameHash;
    protected int keyCount;      // 1 for classic, 2 for LOD...
    protected boolean useSpawn;      // Always false for Diablo...
    protected CDKeyInfo[] keys;
    protected String gameInfo;
    protected String ownerName;

    public BnetAuthRequest(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.clientToken = byteBuffer.getInt(4);
        this.gameVersion = byteBuffer.getInt(8);
        this.gameHash = byteBuffer.getInt(12);
        this.keyCount = byteBuffer.getInt(16);
        this.useSpawn = byteBuffer.getInt(20) == 1 ? true : false;
        this.keys = new CDKeyInfo[this.keyCount];
        int offset = 24;
        for (int i = 0; i < this.keyCount; i++)
        {
            this.keys[i] = new CDKeyInfo(data, offset);
            offset += 36;
        }
        this.gameInfo = ByteConverter.getString(data, offset);
        this.ownerName = ByteConverter.getString(data, offset + this.gameInfo.length() + 1);
    }

    @Override
    public void process(Proxy proxy)
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        int offset = 24;
        for (int i = 0; i < this.keyCount; i++)
        {
            this.keys[i] = new CDKeyInfo(data, offset);
            offset += 36;
        }

        ChatProxy chatProxy = (ChatProxy) proxy;
        CdKey cdKey = new CdKey("PPRT9EKMTPTD2MBF");
        keys[0].setKey(cdKey.getPublicValue(), cdKey.computeHash(clientToken, chatProxy.getServerToken()));
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CCPacket: ").append("BnetAuthRequest").append("\n");
        stringBuilder.append("clientToken: ").append(clientToken).append("\n");
        stringBuilder.append("gameVersion: ").append(gameVersion).append("\n");
        stringBuilder.append("gameHash: ").append(gameHash).append("\n");
        stringBuilder.append("keyCount: ").append(keyCount).append("\n");
        stringBuilder.append("useSpawn: ").append(useSpawn).append("\n");
        stringBuilder.append("keys: ").append(keys).append("\n");
        stringBuilder.append("gameInfo: ").append(gameInfo).append("\n");
        stringBuilder.append("ownerName: ").append(ownerName).append("\n");
        return stringBuilder.toString();
    }
}
