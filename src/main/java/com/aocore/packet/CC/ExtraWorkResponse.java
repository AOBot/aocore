/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CC;

import com.aocore.packet.CCPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class ExtraWorkResponse extends CCPacket
{
    protected int resultLength;
    protected int client;

    public ExtraWorkResponse(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.client = byteBuffer.getShort(4);
        this.resultLength = byteBuffer.getShort(6);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CCPacket: ").append("ExtraWorkResponse").append("\n");
        stringBuilder.append("resultLength: ").append(resultLength).append("\n");
        stringBuilder.append("client: ").append(client).append("\n");
        return stringBuilder.toString();
    }
}
