/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CC;

import com.aocore.d2data.StartGameFlags;
import com.aocore.packet.CCPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class StartGame extends CCPacket
{
    protected StartGameFlags flags;
    protected String name;
    protected String password;
    protected String statString;

    public StartGame(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.flags = StartGameFlags.valueOf(byteBuffer.getInt(4));
        this.name = ByteConverter.getString(data, 24);
        if ((this.flags.getId() & StartGameFlags.Private.getId()) == StartGameFlags.Private.getId())
            this.password = ByteConverter.getString(data, 25 + this.name.length());
        int offset = 26 + this.name.length() + (this.password == null ? 0 : this.password.length());
        if (data.length > offset + 1)
            this.statString = ByteConverter.getString(data, offset);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CCPacket: ").append("StartGame").append("\n");
        stringBuilder.append("flags: ").append(flags).append("\n");
        stringBuilder.append("name: ").append(name).append("\n");
        stringBuilder.append("password: ").append(password).append("\n");
        stringBuilder.append("statString: ").append(statString).append("\n");
        return stringBuilder.toString();
    }
}
