/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CC;

import com.aocore.d2data.BattleNetClient;
import com.aocore.packet.CCPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class NotifyJoin extends CCPacket
{
    protected BattleNetClient client;
    protected int version;
    protected String name;
    protected String password;

    public NotifyJoin(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.client = BattleNetClient.valueOf(byteBuffer.getInt(4));
        this.version = byteBuffer.getInt(8);
        this.name = ByteConverter.getString(data, 12);
        this.password = ByteConverter.getString(data, 13 + this.name.length());
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CCPacket: ").append("NotifyJoin").append("\n");
        stringBuilder.append("client: ").append(client).append("\n");
        stringBuilder.append("version: ").append(version).append("\n");
        stringBuilder.append("name: ").append(name).append("\n");
        stringBuilder.append("password: ").append(password).append("\n");
        return stringBuilder.toString();
    }
}
