/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CC;

import com.aocore.d2data.BattleNetClient;
import com.aocore.d2data.BattleNetPlatform;
import com.aocore.packet.CCPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class DisplayAd extends CCPacket
{
    protected BattleNetPlatform platform;
    protected BattleNetClient client;
    protected int id;
    protected String filename;
    protected String url;

    public DisplayAd(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.platform = BattleNetPlatform.valueOf(byteBuffer.getInt(4));
        this.client = BattleNetClient.valueOf(byteBuffer.getInt(8));
        this.id = byteBuffer.getInt(12);
        if (data[16] != 0)
            this.filename = ByteConverter.getString(data, 16);
        int offset = 17 + (this.filename == null ? 0 : this.filename.length());
        if (data[offset] != 0)
            this.url = ByteConverter.getString(data, offset);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CCPacket: ").append("DisplayAd").append("\n");
        stringBuilder.append("platform: ").append(platform).append("\n");
        stringBuilder.append("client: ").append(client).append("\n");
        stringBuilder.append("id: ").append(id).append("\n");
        stringBuilder.append("filename: ").append(filename).append("\n");
        stringBuilder.append("url: ").append(url).append("\n");
        return stringBuilder.toString();
    }
}
