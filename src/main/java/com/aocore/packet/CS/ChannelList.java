/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CS;

import com.aocore.packet.CSPacket;
import com.aocore.util.ByteConverter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author derek
 */
public class ChannelList extends CSPacket
{
    protected List<String> channels;

    public ChannelList(byte... data)
    {
        super(data);

        channels = new ArrayList<String>();
        int offset = 4;
        int count = 0;
        while (offset < data.length - 1)
        {
            channels.add(ByteConverter.getString(data, offset));
            offset += channels.get(count).length() + 1;
            count++;
        }
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CSPacket: ").append("ChannelList").append("\n");
        stringBuilder.append("channels: ").append(channels).append("\n");
        return stringBuilder.toString();
    }
}
