/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CS;

import com.aocore.d2data.RealmInfo;
import com.aocore.packet.CSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class QueryRealmsResponse extends CSPacket
{
    protected int unknown;
    protected int count;
    protected RealmInfo[] realms;

    public QueryRealmsResponse(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.unknown = byteBuffer.getInt(4);
        this.count = byteBuffer.getInt(8);
        this.realms = new RealmInfo[this.count];
        int offset = 12;
        for (int i = 0; i < this.count; i++)
        {
            this.realms[i] = new RealmInfo(data, offset);
            offset += 6 + this.realms[i].getName().length() + this.realms[i].getDescription().length();
        }
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CSPacket: ").append("QueryRealmsResponse").append("\n");
        stringBuilder.append("unknown: ").append(unknown).append("\n");
        stringBuilder.append("count: ").append(count).append("\n");
        stringBuilder.append("realms: ").append(realms).append("\n");
        return stringBuilder.toString();
    }
}
