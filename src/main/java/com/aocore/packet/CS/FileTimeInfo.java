/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CS;

import com.aocore.packet.CSPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;

/**
 *
 * @author derek
 */
public class FileTimeInfo extends CSPacket
{
    protected int requestID;
    protected int unknown;
    protected Date filetime;
    protected String filename;

    public FileTimeInfo(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.requestID = byteBuffer.getInt(4);
        this.unknown = byteBuffer.getInt(8);
        this.filetime = new Date(byteBuffer.getLong(12));
        this.filename = ByteConverter.getString(data, 20);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CSPacket: ").append("FileTimeInfo").append("\n");
        stringBuilder.append("requestID: ").append(requestID).append("\n");
        stringBuilder.append("unknown: ").append(unknown).append("\n");
        stringBuilder.append("filetime: ").append(filetime).append("\n");
        stringBuilder.append("filename: ").append(filename).append("\n");
        return stringBuilder.toString();
    }
}
