/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CS;

import com.aocore.packet.CSPacket;
import com.aocore.proxy.ChatProxy;
import com.aocore.proxy.Proxy;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;

/**
 *
 * @author derek
 */
public class BnetConnectionResponse extends CSPacket
{
    protected int logonType;
    protected int serverToken;
    protected int udpValue;
    protected Date versionFileTime;
    protected String versionFileName;
    protected String versionFormulae;

    public BnetConnectionResponse(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.logonType = byteBuffer.getInt(4);
        this.serverToken = byteBuffer.getInt(8);
        this.udpValue = byteBuffer.getInt(12);
        this.versionFileTime = new Date(byteBuffer.getLong(16));
        this.versionFileName = ByteConverter.getString(data, 24);
        this.versionFormulae = ByteConverter.getString(data, 25 + this.versionFileName.length());
    }

    @Override
    public void process(Proxy proxy)
    {
        ChatProxy chatProxy = (ChatProxy) proxy;
        chatProxy.setServerToken(serverToken);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CSPacket: ").append("BnetConnectionResponse").append("\n");
        stringBuilder.append("logonType: ").append(logonType).append("\n");
        stringBuilder.append("serverToken: ").append(serverToken).append("\n");
        stringBuilder.append("udpValue: ").append(udpValue).append("\n");
        stringBuilder.append("versionFileTime: ").append(versionFileTime).append("\n");
        stringBuilder.append("versionFileName: ").append(versionFileName).append("\n");
        stringBuilder.append("versionFormulae: ").append(versionFormulae).append("\n");
        return stringBuilder.toString();
    }
}
