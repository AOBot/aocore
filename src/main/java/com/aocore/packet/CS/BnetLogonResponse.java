/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CS;

import com.aocore.d2data.BnetLogonResult;
import com.aocore.packet.CSPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class BnetLogonResponse extends CSPacket
{
    protected BnetLogonResult result;
    protected String reason;

    public BnetLogonResponse(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.result = BnetLogonResult.valueOf(byteBuffer.getInt(4));
        if (data.length > 8)
            this.reason = ByteConverter.getString(data, 8);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CSPacket: ").append("BnetLogonResponse").append("\n");
        stringBuilder.append("result: ").append(result).append("\n");
        stringBuilder.append("reason: ").append(reason).append("\n");
        return stringBuilder.toString();
    }
}
