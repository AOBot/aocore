/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CS;

import com.aocore.d2data.RealmLogonResult;
import com.aocore.packet.CSPacket;
import com.aocore.proxy.Proxy;
import com.aocore.util.ByteConverter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 *
 * @author derek
 */
public class RealmLogonResponse extends CSPacket
{
    protected int cookie;
    protected RealmLogonResult result;
    protected int realmServerIP = -1;
    protected int realmServerPort = -1;
    protected String username = null;

    public RealmLogonResponse(byte... data)
    {
        super(data);

        ByteBuffer leByteBuffer = ByteBuffer.wrap(data);
        ByteBuffer beByteBuffer = ByteBuffer.wrap(data);
        leByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        beByteBuffer.order(ByteOrder.BIG_ENDIAN);
        this.cookie = leByteBuffer.getInt(4);
        if (this.data.length < 75)
        {
            this.result = RealmLogonResult.valueOf(leByteBuffer.getInt(8));
            return;
        }
        this.result = RealmLogonResult.Success;

        // 3 * DWORD = MCP Chunk DWORDS 2 - 4
        this.realmServerIP = leByteBuffer.getInt(20);
        this.realmServerPort = beByteBuffer.getShort(24);
        // 00, 00 (port is actually a DWORD...)
        // 12 * DWORD = MCP Chunk DWORDS 5 - 16
        this.username = ByteConverter.getString(data, 76);
    }

    @Override
    public void process(Proxy proxy)
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        //byteBuffer.order(ByteOrder.LITTLE_ENDIAN);

        byte[] ip = new byte[] { byteBuffer.get(20), byteBuffer.get(21), byteBuffer.get(22), byteBuffer.get(23) };
        short port = byteBuffer.getShort(24);
        byte[] hash = new byte[64];

        byteBuffer.position(4);
        byteBuffer.get(hash, 0, 16);
        byteBuffer.position(28);
        byteBuffer.get(hash, 16, 48);

        try
        {
            InetSocketAddress address = new InetSocketAddress(InetAddress.getByAddress(ip), (int) port);
            proxy.getAwesomO().setRealmAddress(address);
            Proxy.ttt.put(Arrays.hashCode(hash), proxy.getAwesomO());

            System.err.println("hash: " + Arrays.hashCode(hash));
            System.err.println("ip: " + address.getHostName());
            System.err.println("port: " + port);
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
        }

        byteBuffer.put(20, (byte) 192);
        byteBuffer.put(21, (byte) 168);
        byteBuffer.put(22, (byte) 1);
        byteBuffer.put(23, (byte) 145);
        byteBuffer.putShort(24, (short) 6113);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CSPacket: ").append("RealmLogonResponse").append("\n");
        stringBuilder.append("cookie: ").append(cookie).append("\n");
        stringBuilder.append("result: ").append(result).append("\n");
        stringBuilder.append("realmServerIP: ").append(realmServerIP).append("\n");
        stringBuilder.append("realmServerPort: ").append(realmServerPort).append("\n");
        stringBuilder.append("username: ").append(username).append("\n");
        return stringBuilder.toString();
    }
}
