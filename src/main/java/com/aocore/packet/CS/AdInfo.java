/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CS;

import com.aocore.packet.CSPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;

/**
 *
 * @author derek
 */
public class AdInfo extends CSPacket
{
    protected int id;
    protected String extension;
    protected Date timestamp;
    protected String filename;
    protected String url;

    public AdInfo(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.id = byteBuffer.getInt(4);
        this.extension = ByteConverter.getString(data, 8, 4);
        this.timestamp = new Date(byteBuffer.getLong(12));
        this.filename = ByteConverter.getString(data, 20);
        this.url = ByteConverter.getString(data, 21 + this.filename.length());
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CSPacket: ").append("AdInfo").append("\n");
        stringBuilder.append("extension: ").append(extension).append("\n");
        stringBuilder.append("timestamp: ").append(timestamp).append("\n");
        stringBuilder.append("filename: ").append(filename).append("\n");
        stringBuilder.append("url: ").append(url).append("\n");
        return stringBuilder.toString();
    }
}
