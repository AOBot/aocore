/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CS;

import com.aocore.d2data.NewsEntry;
import com.aocore.packet.CSPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;

/**
 *
 * @author derek
 */
public class NewsInfo extends CSPacket
{
    protected int count;
    protected Date lastLogon;
    protected Date oldestEntry;
    protected Date newestEntry;
    protected NewsEntry[] entries;

    public NewsInfo(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.count = data[4];
        this.lastLogon = new Date(byteBuffer.getInt(5));
        this.oldestEntry = new Date(byteBuffer.getInt(9));
        this.newestEntry = new Date(byteBuffer.getInt(13));
        this.entries = new NewsEntry[this.count];
        int offset = 17;
        for (int i = 0; i < this.entries.length; i++)
        {
            this.entries[i] = new NewsEntry(data, offset);
            offset += 5 + this.entries[i].getContent().length();
        }
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CSPacket: ").append("NewsInfo").append("\n");
        stringBuilder.append("count: ").append(count).append("\n");
        stringBuilder.append("lastLogon: ").append(lastLogon).append("\n");
        stringBuilder.append("oldestEntry: ").append(oldestEntry).append("\n");
        stringBuilder.append("newestEntry: ").append(newestEntry).append("\n");
        stringBuilder.append("entries: ").append(entries).append("\n");
        return stringBuilder.toString();
    }
}
