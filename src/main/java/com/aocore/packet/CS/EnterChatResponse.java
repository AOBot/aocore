/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.packet.CS;

import com.aocore.d2data.BattleNetCharacter;
import com.aocore.d2data.BattleNetClient;
import com.aocore.d2data.CharacterFlags;
import com.aocore.d2data.CharacterTitle;
import com.aocore.packet.CSPacket;
import com.aocore.util.ByteConverter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author derek
 */
public class EnterChatResponse extends CSPacket
{
    protected String username;
    protected BattleNetClient client;
    protected String realm;
    protected String account;
    protected String name;

    protected int clientVersion = -1;
    protected BattleNetCharacter characterType = BattleNetCharacter.Unknown;
    protected int characterLevel = -1;
    protected CharacterFlags characterFlags = CharacterFlags.None;
    protected int characterAct = -1;
    protected CharacterTitle characterTitle = CharacterTitle.None;

    public EnterChatResponse(byte... data)
    {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.username = ByteConverter.getString(data, 4);
        int index = 5 + this.username.length();
        this.client = BattleNetClient.valueOf(byteBuffer.getInt(index));
        if (data[index += 4] == 0)
        {
            // StatString is not always present...
            this.account = ByteConverter.getString(data, index + 1);
            return;
        }
        this.realm = ByteConverter.getString(data, index, (byte)44); // 44 == ','
        index += 1 + this.realm.length();
        this.name = ByteConverter.getString(data, index, (byte)44); // 44 == ','
        index += 1 + this.name.length();
        int length = ByteConverter.getByteOffset(data, 0, index);
        this.account = ByteConverter.getString(data, index + length + 1);

        if (this.client == BattleNetClient.Diablo2LoD)
            this.characterFlags = CharacterFlags.valueOf(this.characterFlags.getId() | CharacterFlags.Expansion.getId());

//        StatString.ParseD2StatString(data, index, //length,
//        ref this.clientVersion, ref this.characterType, ref this.characterLevel,
//        ref this.characterFlags, ref this.characterAct, ref this.characterTitle);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CSPacket: ").append("EnterChatResponse").append("\n");
        stringBuilder.append("username: ").append(username).append("\n");
        stringBuilder.append("client: ").append(client).append("\n");
        stringBuilder.append("realm: ").append(realm).append("\n");
        stringBuilder.append("account: ").append(account).append("\n");
        stringBuilder.append("name: ").append(name).append("\n");
        stringBuilder.append("clientVersion: ").append(clientVersion).append("\n");
        stringBuilder.append("characterType: ").append(characterType).append("\n");
        stringBuilder.append("characterLevel: ").append(characterLevel).append("\n");
        stringBuilder.append("characterFlags: ").append(characterFlags).append("\n");
        stringBuilder.append("characterAct: ").append(characterAct).append("\n");
        stringBuilder.append("characterTitle: ").append(characterTitle).append("\n");
        return stringBuilder.toString();
    }
}
