package com.aocore.packet;

import com.aocore.proxy.Proxy;
import java.nio.ByteBuffer;

/**
 *
 * @author derek
 */
public abstract class Packet
{
    protected byte[] data;

    public Packet(byte... data)
    {
        this.data = data;
    }

    public byte[] getData()
    {
        return data;
    }

    public void setData(byte... data)
    {
        this.data = data;
    }

    public void process(Proxy proxy)
    {
    }

    public void write(ByteBuffer byteBuffer)
    {
        byteBuffer.put(data);
    }

    public String getBytes()
    {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < data.length; i++)
        {
            if (i > 0)
            {
                result.append(", ");
            }
            result.append(String.format("0x%02X", data[i]));
        }
        return result.toString();
    }
}
