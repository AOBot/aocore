package com.aocore;

import com.aocore.awesomo.api.events.GameOverEvent;
import com.aocore.awesomo.api.events.GameLogonSuccessEvent;
import com.aocore.packet.GS.GameLogonSuccess;
import com.aocore.packet.GS.GameOver;
import com.aocore.proxy.ChatProxy;
import com.aocore.proxy.GameProxy;
import com.aocore.proxy.RealmProxy;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author derek
 */
public class Core
{
    private enum Listener
    {
        CHAT, REALM, GAME
    }

    private Selector selector = null;
    private ServerSocketChannel chat = null;
    private ServerSocketChannel realm = null;
    private ServerSocketChannel game = null;

    public Core() throws IOException
    {
        // create a new selector
        selector = Selector.open();

        // create a new chat channel
        chat = ServerSocketChannel.open();
        chat.configureBlocking(false);
        chat.socket().bind(new InetSocketAddress(6112));

        // create a new realm channel
        realm = ServerSocketChannel.open();
        realm.configureBlocking(false);
        realm.socket().bind(new InetSocketAddress(6113));

        // create a new game channel
        game = ServerSocketChannel.open();
        game.configureBlocking(false);
        game.socket().bind(new InetSocketAddress(4000));
    }

    public void startServer() throws IOException
    {
        chat.register(selector, SelectionKey.OP_ACCEPT, Listener.CHAT);
        realm.register(selector, SelectionKey.OP_ACCEPT, Listener.REALM);
        game.register(selector, SelectionKey.OP_ACCEPT, Listener.GAME);

        while (selector.select() > 0)
        {
            Iterator<SelectionKey> it = selector.selectedKeys().iterator();

            while (it.hasNext())
            {
                SelectionKey key = it.next();
                it.remove();

                if (key.isAcceptable())
                {
                    ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
                    SocketChannel socketChannel = serverSocketChannel.accept();

                    switch ((Listener) key.attachment())
                    {
                        case CHAT:
                        {
                            System.out.println("CHAT");
                            ChatProxy chatProxy = new ChatProxy(socketChannel);
                            chatProxy.start();
                            break;
                        }
                        case REALM:
                        {
                            System.out.println("REALM");
                            RealmProxy realmProxy = new RealmProxy(socketChannel);
                            realmProxy.start();
                            break;
                        }
                        case GAME:
                        {
                            System.out.println("GAME");
                            GameProxy gameProxy = new GameProxy(socketChannel);
                            gameProxy.start();
                            break;
                        }
                    }
                }
            }
        }
    }

    public static void main(String[] args) throws IOException
    {
        try
        {
            new Core().startServer();
        }
        catch (IOException e)
        {
            e.printStackTrace(System.err);
            System.exit(-1);
        }
    }
}
