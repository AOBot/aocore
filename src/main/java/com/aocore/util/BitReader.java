/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.util;

/**
 *
 * @author derek
 */
public class BitReader
{
    private byte[] data;
    private int position;
    private int mask;

    public BitReader(byte[] data)
    {
        this(data, 0);
    }

    public BitReader(byte[] data, int position)
    {
        this(data, position, 0x80);
    }

    public BitReader(byte[] data, int position, int mask)
    {
        this.data = data;
        this.position = position;
        this.mask = mask;
    }

    public boolean getBoolean(int bits)
    {
        return true;
    }

    public short getShort(int bits)
    {
        return 0;
    }

    public int getInt(int bits)
    {
        return 0;
    }
}
