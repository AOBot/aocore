/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum GameQuestState
{
    /// <summary>
    /// The creator had not completed the quest and it has not been done in this game yet.
    /// </summary>
    Open(0),
    /// <summary>
    /// The creator has already completed this quest.
    /// </summary>
    Closed(0x8000),
    /// <summary>
    /// The quest has already been completed in game.
    /// </summary>
    Completed(0x2000);

    private final int id;

    private GameQuestState(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static GameQuestState valueOf(int id) throws IllegalArgumentException
    {
        for (GameQuestState gameQuestState : values())
        {
            if (gameQuestState.getId() == id)
            {
                return gameQuestState;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid GameQuestState");
    }
}
