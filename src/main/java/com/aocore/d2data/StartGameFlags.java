/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum StartGameFlags
{
    Public(0),
    Private(1);
    //0x02: Game is full
    //0x04: Game contains players (other than creator)
    //0x08: Game is in progress

    private final int id;

    private StartGameFlags(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static StartGameFlags valueOf(int id) throws IllegalArgumentException
    {
        for (StartGameFlags startGameFlags : values())
        {
            if (startGameFlags.getId() == id)
            {
                return startGameFlags;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid StartGameFlags");
    }
}
