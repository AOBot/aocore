/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum QuestStanding
{
    Complete(1),
    RewardPending(2),
    Started(4),
    LeftTownWhileStarted(8),
    EnteredFirstArea(0x10),
    Event0x20(0x20),
    Event0x40(0x40),
    Event0x80(0x80);

    private final int id;

    private QuestStanding(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static QuestStanding valueOf(int id) throws IllegalArgumentException
    {
        for (QuestStanding questStanding : values())
        {
            if (questStanding.getId() == id)
            {
                return questStanding;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid QuestStanding");
    }
}
