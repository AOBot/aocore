/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum UnitType
{
    /// <summary>
    /// Any player character, including yours.
    /// </summary>
    Player(0),
    /// <summary>
    /// Any non player character, including town folks and monsters.
    /// </summary>
    NPC(1),
    /// <summary>
    /// Most generic game objects, such as chests, dummy objects, etc.
    /// </summary>
    GameObject(2),
    /// <summary>
    /// Any kind of missiles, even those fired by the French.
    /// </summary>
    Missile(3),
    /// <summary>
    /// Item units.
    /// </summary>
    Item(4),
    /// <summary>
    /// Doorways, stairs, etc used to change area.
    /// </summary>
    Warp(5),        // Room tile ?

    Invalid(6),
    NotApplicable(0xFF);

    private final int id;

    private UnitType(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static UnitType valueOf(int id) throws IllegalArgumentException
    {
        for (UnitType unitType : values())
        {
            if (unitType.getId() == id)
            {
                return unitType;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid UnitType");
    }
}
