/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum ChatEventType
{
    /// <summary>
    /// Received for every user when you join a channel.
    /// <para>Also sent when a user requires an update to his statstring.</para>
    /// </summary>
    ChannelUser(1),
    /// <summary>
    /// Someone joins the channel you're currently in.
    /// </summary>
    ChannelJoin(2),
    /// <summary>
    /// Someone leaves the channel you're currently in.
    /// </summary>
    ChannelLeave(3),
    /// <summary>
    /// Received whisper message.
    /// </summary>
    ReceiveWhisper(4),
    /// <summary>
    /// Received when someone talks in the channel you're currently in.
    /// </summary>
    ChannelMessage(5),
    /// <summary>
    /// Server information broadcast.
    /// </summary>
    ServerBroadcast(6),
    /// <summary>
    /// Received when you join a channel (channel's name and flags.)
    /// </summary>
    ChannelInfo(7),
    /// <summary>
    /// Update a user's flags.
    /// </summary>
    UserFlags(9),
    /// <summary>
    /// Sent whisper message receipt.
    /// </summary>
    WhisperSent(0x0a),
    ChannelFull(0x0d),
    ChannelDoesNotExist(0x0e),
    ChannelRestricted(0x0f),
    /// <summary>
    /// Account is the one you are logged on. Used to be the BattleNetAdministrator who sent the message.
    /// </summary>
    Broadcast(0x12),
    Error(0x13),
    Emote(0x17);

    private final int id;

    private ChatEventType(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static ChatEventType valueOf(int id) throws IllegalArgumentException
    {
        for (ChatEventType chatEventType : values())
        {
            if (chatEventType.getId() == id)
            {
                return chatEventType;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid ChatEventType");
    }
}
