/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum GameSound
{
    /// <summary>
    /// Item specific sound triggered when an item is swap or picked up.
    /// </summary>
    Pickup(1),
    /// <summary>
    /// Masochistic Baal has a good laugh because he's one step closer to getting his ass kicked.
    /// </summary>
    BaalLaugh(16),
    /// <summary>
    /// Cannot perform action (pick up item with telekinesis or when there's not enough space in inventory).
    /// </summary>
    Impossible(19),
    HelpMe(25),
    FollowMe(26),
    ThisIsForYou(27),
    ThankYou(28),
    ForgiveMe(29),
    GoodBye(30),
    TimeToDie(31),
    RunAway(32),
    IllPutThatToGoodUse(87);

    private final int id;

    private GameSound(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static GameSound valueOf(int id) throws IllegalArgumentException
    {
        for (GameSound gameSound : values())
        {
            if (gameSound.getId() == id)
            {
                return gameSound;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid GameSound");
    }
}
