/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum InformationMessageType
{
    /// <summary>
    /// Player Has Dropped Due To Time Out
    /// </summary>
    DroppedFromGame(0),
    /// <summary>
    /// Player Has Joined The Game
    /// </summary>
    JoinedGame(2),
    /// <summary>
    /// Player Has Left The Game
    /// </summary>
    LeftGame(3),
    /// <summary>
    /// Player Is Not In The Game (answer from @whisper command)
    /// The server will send you this packet after you have used the @charname command to whisper a player that is not in the game.
    /// </summary>
    NotInGame(4),
    /// <summary>
    /// A Player Has Been Slained
    /// </summary>
    PlayerSlain(6),
    /// <summary>
    /// Player To Player Relations
    /// The server will send you this packet to notify you of any changes that a D2 client would use to set up parties/hostile players/looting etc.
    /// This packet is a very important packet when it comes to party relations and should be the packet you take note of when adding party support into your bot.
    /// TEST: does this mean AboutPlayer etc are unreliable ??
    /// </summary>
    PlayerRelation(7),
    /// <summary>
    /// #### Stones of Jordan Sold to Merchants
    /// </summary>
    SoJsSoldToMerchants(0x11),
    DiabloWalksTheEarth(0x12);

    private final int id;

    private InformationMessageType(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static InformationMessageType valueOf(int id) throws IllegalArgumentException
    {
        for (InformationMessageType informationMessageType : values())
        {
            if (informationMessageType.getId() == id)
            {
                return informationMessageType;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid InformationMessageType");
    }
}
