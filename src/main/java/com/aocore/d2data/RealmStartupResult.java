/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum RealmStartupResult
{
    Success(0),
    NoBattleNetConnection(0x0C),
    InvalidCDKey(0x7E),
    /// <summary>
    /// "Your connection has been temporarily restricted from this realm.
    /// Please try to log in at another time."
    /// </summary>
    TemporaryIPBan(0x7F);

    private final int id;

    private RealmStartupResult(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static RealmStartupResult valueOf(int id) throws IllegalArgumentException
    {
        for (RealmStartupResult realmStartupResult : values())
        {
            if (realmStartupResult.getId() == id)
            {
                return realmStartupResult;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid RealmStartupResult");
    }
}
