/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum EquipmentLocation
{
    NotApplicable(0),
    Helm(1),
    Amulet(2),
    Armor(3),
    RightHand(4),
    LeftHand(5),
    RightHandRing(6),
    LeftHandRing(7),
    Belt(8),
    Boots(9),
    Gloves(10),
    RightHandSwitch(11),
    LeftHandSwitch(12);

    private final int id;

    private EquipmentLocation(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static EquipmentLocation valueOf(int id) throws IllegalArgumentException
    {
        for (EquipmentLocation equipmentLocation : values())
        {
            if (equipmentLocation.getId() == id)
            {
                return equipmentLocation;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid EquipmentLocation");
    }
}
