/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum WaypointsAvailiable
{
    None(0x00L),
    RogueEncampment(0x01L),
    ColdPlains(0x02L),
    StonyField(0x04L),
    DarkWood(0x08L),
    BlackMarsh(0x10L),
    OuterCloister(0x20L),
    JailLevel1(0x40L),
    InnerCloister(0x80L),
    CatacombsLevel2(0x100L),
    LutGholein(0x200L),
    LutGholeinSewersLevel2(0x400L),
    DryHills(0x800L),
    HallsOfTheDeadLevel2(0x1000L),
    FarOasis(0x2000L),
    LostCity(0x4000L),
    PalaceCellarLevel1(0x8000L),
    ArcaneSanctuary (0x10000L),
    CanyonOfTheMagi(0x20000L),
    KurastDocks(0x40000L),
    SpiderForest(0x80000L),
    GreatMarsh(0x100000L),
    FlayerJungle(0x200000L),
    LowerKurast(0x400000L),
    KurastBazaar(0x800000L),
    UpperKurast(0x1000000L),
    Travincial(0x2000000L),
    DuranceOfHateLevel2(0x4000000L),
    ThePandemoniumFortress(0x8000000L),
    CityOfTheDamned(0x10000000L),
    RiverOfFlame(0x20000000L),
    Harrogath(0x40000000L),
    FrigidHighlands(0x80000000L),
    ArreatPlateau(0x100000000L),
    CrystallinePassage(0x200000000L),
    GlacialTrail(0x400000000L),
    HallsOfPain(0x800000000L),
    FrozenTundra(0x1000000000L),
    TheAncientsWay(0x2000000000L),
    WorldstoneKeepLevel2(0x4000000000L);
    private final long id;

    private WaypointsAvailiable(long id)
    {
        this.id = id;
    }

    public long getId()
    {
        return id;
    }

    public static WaypointsAvailiable valueOf(long id) throws IllegalArgumentException
    {
        for (WaypointsAvailiable waypointsAvailiable : values())
        {
            if (waypointsAvailiable.getId() == id)
            {
                return waypointsAvailiable;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid WaypointsAvailiable");
    }
}
