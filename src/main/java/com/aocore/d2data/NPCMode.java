/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum NPCMode
{
    Alive(0x06),
    /// <summary>
    /// Null X and Y means the monster displays a 'in air' dying animation..
    /// </summary>
    Dying(0x08),
    /// <summary>
    /// Dead monsters coming into view as well.
    /// </summary>
    Dead(0x09);

    private final int id;

    private NPCMode(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static NPCMode valueOf(int id) throws IllegalArgumentException
    {
        for (NPCMode npcMode : values())
        {
            if (npcMode.getId() == id)
            {
                return npcMode;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid NPCMode");
    }
}
