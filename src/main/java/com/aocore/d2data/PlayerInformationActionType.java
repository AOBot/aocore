/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum PlayerInformationActionType
{
    Neutral(0),
    Party(2),
    Friendly(4),
    UnFriendly(8),
    Remove(1),
    None(0x80);

    private final int id;

    private PlayerInformationActionType(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static PlayerInformationActionType valueOf(int id) throws IllegalArgumentException
    {
        for (PlayerInformationActionType playerInformationActionType : values())
        {
            if (playerInformationActionType.getId() == id)
            {
                return playerInformationActionType;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid PlayerInformationActionType");
    }
}
