/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum GameObjectInteractType
{
    GeneralObject(0x00),  // Stash, chests, etc.
    Well(0x01),
    HealthShrine(0x02),
    TrappedChest(0x05),
    MonsterChest(0x08),
    ManaShrine(0x0D),
    StaminaShrine(0x0E),
    ExperienceShrine(0x0F),
    FireShrine(0x13),
    RedPortal(0x79),  // Pindle portal... all red portals ?
    LockedChest(0x80),
    /// <summary>
    /// Dummy value because for town portals, the intereact type is the destination area...
    /// </summary>
    TownPortal(0xFF);

    private final int id;

    private GameObjectInteractType(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static GameObjectInteractType valueOf(int id) throws IllegalArgumentException
    {
        for (GameObjectInteractType gameObjectInteractType : values())
        {
            if (gameObjectInteractType.getId() == id)
            {
                return gameObjectInteractType;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid GameObjectInteractType");
    }
}
