/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum GameFlags
{
    /// <summary>
    /// If not present, the packet contains no useful information.
    /// <para>For <see cref="RealmServer.GameList"/>, this means the packet marks the end of listing.</para>
    /// <para>For <see cref="RealmServer.GameInfo"/>, the game was destroyed since listed !?</para>
    /// </summary>
    Valid(4),
    Hardcore(0x800),
    Nightmare(0x1000),
    Hell(0x2000),
    Empty(0x20000),
    Expansion(0x100000),
    Ladder(0x200000),
    ServerDown(0xFFFFFFFF),
    GameDestroyed(0xFFFFFFFE);

    private final int id;

    private GameFlags(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static GameFlags valueOf(int id) throws IllegalArgumentException
    {
        for (GameFlags gameFlags : values())
        {
            if (gameFlags.getId() == id)
            {
                return gameFlags;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid GameFlags");
    }
}
