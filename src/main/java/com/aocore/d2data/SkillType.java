/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum SkillType
{
    Attack(0),
    Kick(1),
    Throw(2),
    Unsummon(3),
    LeftHandThrow(4),
    LeftHandSwing(5),
    MagicArrow(6),
    FireArrow(7),
    InnerSight(8),
    CriticalStrike(9),
    Jab(10),
    ColdArrow(11),
    MultipleShot(12),
    Dodge(13),
    PowerStrike(14),
    PoisonJavelin(15),
    ExplodingArrow(16),
    SlowMissiles(17),
    Avoid(18),
    Impale(19),
    LightningBolt(20),
    IceArrow(21),
    GuidedArrow(22),
    Penetrate(23),
    ChargedStrike(24),
    PlagueJavelin(25),
    Strafe(26),
    ImmolationArrow(27),
    Dopplezon(28),
    Evade(29),
    Fend(30),
    FreezingArrow(31),
    Valkyrie(32),
    Pierce(33),
    LightningStrike(34),
    LightningFury(35),
    FireBolt(36),
    Warmth(37),
    ChargedBolt(38),
    IceBolt(39),
    FrozenArmor(40),
    Inferno(41),
    StaticField(42),
    Telekinesis(43),
    FrostNova(44),
    IceBlast(45),
    Blaze(46),
    FireBall(47),
    Nova(48),
    Lightning(49),
    ShiverArmor(50),
    FireWall(51),
    Enchant(52),
    ChainLightning(53),
    Teleport(54),
    GlacialSpike(55),
    Meteor(56),
    ThunderStorm(57),
    EnergyShield(58),
    Blizzard(59),
    ChillingArmor(60),
    FireMastery(61),
    Hydra(62),
    LightningMastery(63),
    FrozenOrb(64),
    ColdMastery(65),
    AmplifyDamage(66),
    Teeth(67),
    BoneArmor(68),
    SkeletonMastery(69),
    RaiseSkeleton(70),
    DimVision(71),
    Weaken(72),
    PoisonDagger(73),
    CorpseExplosion(74),
    ClayGolem(75),
    IronMaiden(76),
    Terror(77),
    BoneWall(78),
    GolemMastery(79),
    RaiseSkeletalMage(80),
    Confuse(81),
    LifeTap(82),
    PoisonExplosion(83),
    BoneSpear(84),
    Bloodgolem(85),
    Attract(86),
    Decrepify(87),
    BonePrison(88),
    SummonResist(89),
    IronGolem(90),
    LowerResist(91),
    PoisonNova(92),
    BoneSpirit(93),
    Firegolem(94),
    Revive(95),
    Sacrifice(96),
    Smite(97),
    Might(98),
    Prayer(99),
    ResistFire(100),
    HolyBolt(101),
    HolyFire(102),
    Thorns(103),
    Defiance(104),
    ResistCold(105),
    Zeal(106),
    Charge(107),
    BlessedAim(108),
    Cleansing(109),
    ResistLightning(110),
    Vengeance(111),
    BlessedHammer(112),
    Concentration(113),
    HolyFreeze(114),
    Vigor(115),
    Conversion(116),
    HolyShield(117),
    HolyShock(118),
    Sanctuary(119),
    Meditation(120),
    FistOfTheHeavens(121),
    Fanaticism(122),
    Conviction(123),
    Redemption(124),
    Salvation(125),
    Bash(126),
    SwordMastery(127),
    AxeMastery(128),
    MaceMastery(129),
    Howl(130),
    FindPotion(131),
    Leap(132),
    DoubleSwing(133),
    PoleArmMastery(134),
    ThrowingMastery(135),
    SpearMastery(136),
    Taunt(137),
    Shout(138),
    Stun(139),
    DoubleThrow(140),
    IncreasedStamina(141),
    FindItem(142),
    LeapAttack(143),
    Concentrate(144),
    IronSkin(145),
    BattleCry(146),
    Frenzy(147),
    IncreasedSpeed(148),
    BattleOrders(149),
    GrimWard(150),
    Whirlwind(151),
    Berserk(152),
    NaturalResistance(153),
    WarCry(154),
    BattleCommand(155),
    FireHit(156),
    Unholybolt(157),
    Skeletonraise(158),
    MaggotEgg(159),
    ShamanFire(160),
    MagottUp(161),
    MagottDown(162),
    MagottLay(163),
    AndrialSpray(164),
    Jump(165),
    SwarmMove(166),
    Nest(167),
    QuickStrike(168),
    Vampirefireball(169),
    Vampirefirewall(170),
    Vampiremeteor(171),
    GargoyleTrap(172),
    SpiderLay(173),
    VampireHeal(174),
    VampireRaise(175),
    SubMerge(176),
    FetishAura(177),
    FetishInferno(178),
    ZakarumHeal(179),
    Emerge(180),
    Resurrect(181),
    Bestow(182),
    MissileSkill1(183),
    MonsterTeleport(184),
    PrimeLightning(185),
    PrimeBolt(186),
    PrimeBlaze(187),
    PrimeFirewall(188),
    PrimeSpike(189),
    PrimeIceNova(190),
    PrimePoisonBall(191),
    PrimePoisonNova(192),
    DiabloLight(193),
    DiabloCold(194),
    DiabloFire(195),
    FingerMageSpider(196),
    DiabloFirestorm(197),
    DiabloRun(198),
    DiabloPrison(199),
    PoisonBallTrap(200),
    AndyPoisonBolt(201),
    HireableMissile(202),
    DesertTurret(203),
    ArcaneTower(204),
    MonsterBlizzard(205),
    Mosquito(206),
    Cursedballtrapright(207),
    Cursedballtrapleft(208),
    MonsterFrozenArmor(209),
    MonsterBoneArmor(210),
    MonsterBoneSpirit(211),
    MonsterCurseCast(212),
    HellMeteor(213),
    RegurgitatorEat(214),
    MonsterFrenzy(215),
    QueenDeath(216),
    ScrollOfIdentify(217),
    BookOfIdentify(218),
    ScrollOfTownportal(219),
    BookOfTownportal(220),
    Raven(221),
    PoisonCreeper(222),
    Wearwolf(223),
    ShapeShifting(224),
    Firestorm(225),
    OakSage(226),
    SummonSpiritWolf(227),
    Wearbear(228),
    MoltenBoulder(229),
    ArcticBlast(230),
    CycleOfLife(231),
    FeralRage(232),
    Maul(233),
    Fissure(234),
    CycloneArmor(235),
    HeartOfWolverine(236),
    SummonDireWolf(237),
    Rabies(238),
    FireClaws(239),
    Twister(240),
    Vines(241),
    Hunger(242),
    ShockWave(243),
    Volcano(244),
    Tornado(245),
    SpiritOfBarbs(246),
    SummonGrizzly(247),
    Fury(248),
    Armageddon(249),
    Hurricane(250),
    FireBlast(251),
    ClawMastery(252),
    PsychicHammer(253),
    TigerStrike(254),
    DragonTalon(255),
    ShockWeb(256),
    BladeSentinel(257),
    BurstOfSpeed(258),
    FistsOfFire(259),
    DragonClaw(260),
    ChargedBoltSentry(261),
    WakeOfFire(262),
    WeaponBlock(263),
    CloakOfShadows(264),
    CobraStrike(265),
    BladeFury(266),
    Fade(267),
    ShadowWarrior(268),
    ClawsOfThunder(269),
    DragonTail(270),
    LightningSentry(271),
    WakeOfInferno(272),
    MindBlast(273),
    BladesOfIce(274),
    DragonFlight(275),
    DeathSentry(276),
    BladeShield(277),
    Venom(278),
    ShadowMaster(279),
    PhoenixStrike(280),
    WakeOfDestructionSentry(281),
    ImpInferno(282),
    ImpFireball(283),
    BaalTaunt(284),
    BaalCorpseExplode(285),
    BaalMonsterSpawn(286),
    CatapultChargedBall(287),
    CatapultSpikeBall(288),
    SuckBlood(289),
    CryHelp(290),
    HealingVortex(291),
    Teleport2(292),
    Selfresurrect(293),
    VineAttack(294),
    OverseerWhip(295),
    BarbsAura(296),
    WolverineAura(297),
    OakSageAura(298),
    ImpFireMissile(299),
    Impregnate(300),
    SiegeBeastStomp(301),
    MinionSpawner(302),
    CatapultBlizzard(303),
    CatapultPlague(304),
    CatapultMeteor(305),
    BoltSentry(306),
    CorpseCycler(307),
    DeathMaul(308),
    DefenseCurse(309),
    BloodMana(310),
    InfernoSentry2(311),
    DeathSentry2(312),
    SentryLightning(313),
    FenrisRage(314),
    BaalTentacle(315),
    BaalNova(316),
    BaalInferno(317),
    BaalColdMissiles(318),
    MegaDemonInferno(319),
    EvilHutSpawner(320),
    CountessFirewall(321),
    ImpBolt(322),
    HorrorArcticBlast(323),
    DeathSentryLightning(324),
    VineCycler(325),
    BearSmite(326),
    Resurrect2(327),
    BloodLordFrenzy(328),
    BaalTeleport(329),
    ImpTeleport(330),
    BaalCloneTeleport(331),
    ZakarumLightning(332),
    VampireMissile(333),
    MephistoMissile(334),
    DoomKnightMissile(335),
    RogueMissile(336),
    HydraMissile(337),
    NecromageMissile(338),
    MonsterBow(339),
    MonsterFireArrow(340),
    MonsterColdArrow(341),
    MonsterExplodingArrow(342),
    MonsterFreezingArrow(343),
    MonsterPowerStrike(344),
    SuccubusBolt(345),
    MephistoFrostNova(346),
    MonsterIceSpear(347),
    ShamanIce(348),
    DiabloArmageddon(349),
    Delirium(350),
    NihlathakCorpseExplosion(351),
    SerpentCharge(352),
    TrapNova(353),
    UnHolyBoltEx(354),
    ShamanFireEx(355),
    ImpFireMissileEx(356),
    Invalid(0xFFFE),
    None(0xFFFF);

    private final int id;

    private SkillType(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static SkillType valueOf(int id) throws IllegalArgumentException
    {
        for (SkillType skillType : values())
        {
            if (skillType.getId() == id)
            {
                return skillType;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid SkillType");
    }
}
