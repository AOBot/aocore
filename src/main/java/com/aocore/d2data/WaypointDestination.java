/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum WaypointDestination
{
    CloseWaypoint(0x00),
    RogueEncampment(0x01),
    ColdPlains(0x03),
    StonyField(0x04),
    DarkWood(0x05),
    BlackMarsh(0x06),
    OuterCloister(0x1B),
    JailLevel1(0x1D),
    InnerCloister(0x20),
    CatacombsLevel2(0x23),
    LutGholein(0x28),
    LutGholeinSewersLevel2(0x30),
    DryHills(0x2A),
    HallsOfTheDeadLevel2(0x39),
    FarOasis(0x2B),
    LostCity(0x2C),
    PalaceCellarLevel1(0x34),
    ArcaneSanctuary(0x4A),
    CanyonOfTheMagi(0x2E),
    KurastDocks(0x4B),
    SpiderForest(0x4C),
    GreatMarsh(0x4D),
    FlayerJungle(0x4E),
    LowerKurast(0x4F),
    KurastBazaar(0x50),
    UpperKurast(0x51),
    Travincial(0x53),
    DuranceOfHateLevel2(0x65),
    ThePandemoniumFortress(0x67),
    CityOfTheDamned(0x6A),
    RiverOfFlame(0x6B),
    Harrogath(0x6D),
    FrigidHighlands(0x6F),
    ArreatPlateau(0x70),
    CrystallinePassage(0x71),
    GlacialTrail(0x73),
    HallsOfPain(0x7B),
    FrozenTundra(0x75),
    TheAncientsWay(0x76),
    WorldstoneKeepLevel2(0x81);

    private final int id;

    private WaypointDestination(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static WaypointDestination valueOf(int id) throws IllegalArgumentException
    {
        for (WaypointDestination waypointDestination : values())
        {
            if (waypointDestination.getId() == id)
            {
                return waypointDestination;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid WaypointDestination");
    }
}
