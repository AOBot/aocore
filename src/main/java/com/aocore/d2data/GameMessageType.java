/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum GameMessageType
{
    GameMessage(1),
    GameWhisper(2),
    OverheadMessage(5),
    GameWhisperReceipt(6);

    private final int id;

    private GameMessageType(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static GameMessageType valueOf(int id) throws IllegalArgumentException
    {
        for (GameMessageType gameMessageType : values())
        {
            if (gameMessageType.getId() == id)
            {
                return gameMessageType;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid GameMessageType");
    }
}
