/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public class CharacterBaseInfo
{
    private final String Name;
    private final CharacterClass characterClass;
    private final int Level;

    public CharacterBaseInfo(String Name, CharacterClass characterClass, int Level)
    {
        this.Name = Name;
        this.characterClass = characterClass;
        this.Level = Level;
    }

    public int getLevel()
    {
        return Level;
    }

    public String getName()
    {
        return Name;
    }

    public CharacterClass getCharacterClass()
    {
        return characterClass;
    }
}
