/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author derek
 */
public class CDKeyInfo
{
    private final byte[] data;
    private final int offset;
    /// <summary>
    /// Always 16 for Diablo II...
    /// </summary>
    public final int length;
    public final int productValue;
    public int publicValue;
    /// <summary>
    /// Always 0...
    /// </summary>
    public final int unknown;
    public byte[] hash;

    public CDKeyInfo(byte[] data, int offset)
    {
        this.data = data;
        this.offset = offset;

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        this.length = byteBuffer.getInt(offset);
        this.productValue = byteBuffer.getInt(offset + 4);
        this.publicValue = byteBuffer.getInt(offset + 8);
        this.unknown = byteBuffer.getInt(offset + 12);
        this.hash = Arrays.copyOfRange(data, offset + 16, offset + 16 + length);
    }

    public void setKey(int publicValue, byte[] hash)
    {
        this.publicValue = publicValue;
        this.hash = hash;

        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        byteBuffer.putInt(offset + 8, publicValue);
        byteBuffer.position(offset + 16);
        byteBuffer.put(hash);
    }
}
