/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum CharacterTitle
{
    // Classic, male, softcore
    Nooblar(0),
    Sir(1),
    Lord(2),
    Baron(3),

    // Classic, female, softcore
    Nooblette(0x100),
    Dame(0x101),
    Lady(0x102),
    Baroness(0x103),

    // Classic, male, hardcore
    CourageousNooblar(4),
    Count(5),
    Duke(6),
    King(7),

    // Classic, female, hardcore
    CourageousNooblette(0x104),
    Countess(0x105),
    Duchess(0x106),
    Queen(0x107),

    // Expansion, male, softcore
    DoublePlusNooblar(0x20),
    Slayer(0x21),
    Champion(0x22),
    Patriarch(0x23),

    // Expansion, female, softcore
    DoublePlusNooblette(0x120),
    SlayerF(0x121),		// Slayer
    ChampionF(0x122),		// Champion
    Matriarch(0x123),

    // Expansion, male, hardcore
    NooblarWhoLikesChicken(0x24),
    Destroyer(0x25),
    Conquerer(0x26),
    Guardian(0x27),

    // Expansion, female, hardcore
    NoobletteWhoLikesChicken(0x124),
    DestroyerF(0x125),	// Destroyer
    ConquererF(0x126),	// Conquerer
    GuardianF(0x127),	// Guardian

    None(0xFFFF);

    private final int id;

    private CharacterTitle(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static CharacterTitle valueOf(int id) throws IllegalArgumentException
    {
        for (CharacterTitle characterTitle : values())
        {
            if (characterTitle.getId() == id)
            {
                return characterTitle;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid CharacterTitle");
    }
}
