/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum QuestType
{
    SpokeToWarriv(0),
    DenOfEvil(1),
    SistersBurialGrounds(2),
    ToolsOfTheTrade(3),
    TheSearchForCain(4),
    TheForgottenTower(5),
    SistersToTheSlaughter(6),
    CanGoToAct2(7),
    SpokeToJerhyn(8),
    RadamentsLair(9),
    TheHoradricStaff(10),
    TaintedSun(11),
    ArcaneSanctuary(12),
    TheSummoner(13),
    TheSevenTombs(14),
    CanGoToAct3(15),
    SpokeToHratli(16),
    LamEsensTome(17),
    KhalimsWill(18),
    BladeOfTheOldReligion(19),
    TheGoldenBird(20),
    TheBlackenedTemple(21),
    TheGuardian(22),
    CanGoToAct4(23),
    SpokeToTyrael(24),
    TheFallenAngel(25),
    TerrorsEnd(26),
    HellsForge(27),
    /* TODO: extra sub quests here !!!
    1A   a4q3   The Hellforge
    1B          (referred to)
                (also items "hfh" and "dss" -- "hfh" is Hellforge Hammer)
                 but "dss" not in item tables)
    1C          (referred to in Hellforge code)
    1D          <<not used???>>
    1E   a2q7   vestigial reference???
    1F   a2q8   vestigial reference???
    20   a3q7   vestigial reference???
    21          <<not used???>>
    22          <<not used???>>
    */
    CanGoToAct5(28),
    // TODO: extra sub quests here !!!
    TheSecretCowLevel(32),
    // TODO: extra sub quests here !!!
    SeigeOnHarrogath(35),
    RescueOnMountArreat(36),
    PrisonOfIce(37),
    BetrayalOfHarrogath(38),
    RiteOfPassage(39),
    EveOfDestruction(40);

    private final int id;

    private QuestType(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static QuestType valueOf(int id) throws IllegalArgumentException
    {
        for (QuestType questType : values())
        {
            if (questType.getId() == id)
            {
                return questType;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid QuestType");
    }
}
