/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum TownPortalState
{
    None(0),
    Unknown1(1),
    IsOtherSide(2),
    Used(4);

    private final int id;

    private TownPortalState(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static TownPortalState valueOf(int id) throws IllegalArgumentException
    {
        for (TownPortalState townPortalState : values())
        {
            if (townPortalState.getId() == id)
            {
                return townPortalState;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid TownPortalState");
    }
}
