/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum BattleNetClient
{
    Unknown(0),
    DiabloShareware(0x44534852),
    Diablo(0x4452544c),
    Diablo2(0x44324456),
    Diablo2LoD(0x44325850),
    Warcraft2(0x5732424e),
    Warcraft3(0x57415233),
    Warcraft3FrozenThrone(0x57335850),
    StarcraftShareware(0x53534852),
    StarcraftJapanese(0x4a535452),
    Starcraft(0x53544152),
    StarcraftBroodWar(0x53455850),
    ChatBot(0x43484154);

    private final int id;

    private BattleNetClient(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static BattleNetClient valueOf(int id) throws IllegalArgumentException
    {
        for (BattleNetClient battleNetClient : values())
        {
            if (battleNetClient.getId() == id)
            {
                return battleNetClient;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid BattleNetClient");
    }
}
