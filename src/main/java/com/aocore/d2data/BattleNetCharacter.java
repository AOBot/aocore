/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum BattleNetCharacter
{
    // Diablo 2
    Amazon(0),
    Sorceress(1),
    Necromancer(2),
    Paladin(3),
    Barbarian(4),
    // Diablo 2 LoD
    Druid(5),
    Assassin(6),

    OpenCharacter(7),

    // Diablo 1
    Warrior(8),
    Rogue(9),
    Sorcerer(10),

    Unknown1(11),
    Unknown2Grey(12),
    Unknown3Grey(13),

    StarcraftMarine(14),
    BroodWarMedic(15),
    WarcraftIIGrunt(16),
    BlizzardRep(17),
    Moderator(18),
    Sysop(19),
    Referee(20),
    Chat(21),
    Speaker(22),
    Unknown(23);

    private final int id;

    private BattleNetCharacter(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static BattleNetCharacter valueOf(int id) throws IllegalArgumentException
    {
        for (BattleNetCharacter battleNetCharacter : values())
        {
            if (battleNetCharacter.getId() == id)
            {
                return battleNetCharacter;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid BattleNetCharacter");
    }
}
