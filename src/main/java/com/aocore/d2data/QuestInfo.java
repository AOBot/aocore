/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public class QuestInfo
{
    private final QuestType questType;
    private final QuestState questState;
    private final QuestStanding questStanding;

    public QuestInfo(QuestType questType, QuestState questState, QuestStanding questStanding)
    {
        this.questType = questType;
        this.questState = questState;
        this.questStanding = questStanding;
    }

    public QuestInfo(int questType, int questState, int questStanding)
    {
        this.questType = QuestType.valueOf(questType);
        this.questState = QuestState.valueOf(questState);
        this.questStanding = QuestStanding.valueOf(questStanding);
    }

    public QuestType getQuestType()
    {
        return questType;
    }

    public QuestState getQuestState()
    {
        return questState;
    }

    public QuestStanding getQuestStanding()
    {
        return questStanding;
    }
}
