/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum JoinGameResult
{
    /// <summary>
    /// Terminate the connection with the Realm Server and initiate with Game Server.
    /// </summary>
    Success(0),
    PasswordIncorrect(0x29),
    GameDoesNotExist(0x2A),
    GameFull(0x2B),
    /// <summary>
    /// You do not meet the level requirements for this game.
    /// </summary>
    LevelRequirementsNotMet(0x2C),
    /// <summary>
    /// A dead hardcore character cannot join a game.
    /// </summary>
    DeadHardcoreCharacter(0x6E),
    /// <summary>
    /// A non-hardcore character cannot join a game created by a hardcore character.
    /// </summary>
    UnableToJoinHardcoreGame(0x71),
    UnableToJoinNightmareGame(0x73),
    UnableToJoinHellGame(0x74),
    /// <summary>
    /// A non-expansion character cannot join a game created by an expansion character.
    /// </summary>
    UnableToJoinExpansionGame(0x78),
    /// <summary>
    /// An expansion character cannot join a game created by a non-expansion character.
    /// </summary>
    UnableToJoinClassicGame(0x79),
    /// <summary>
    /// A non-ladder character cannot join a game created by a Ladder character.
    /// </summary>
    UnableToJoinLadderGame(0x7D);

    private final int id;

    private JoinGameResult(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static JoinGameResult valueOf(int id) throws IllegalArgumentException
    {
        for (JoinGameResult joinGameResult : values())
        {
            if (joinGameResult.getId() == id)
            {
                return joinGameResult;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid JoinGameResult");
    }
}
