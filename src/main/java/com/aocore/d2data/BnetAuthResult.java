/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum BnetAuthResult
{
    /// <summary>
    /// Passed challenge sucessfully.
    /// </summary>
    Success(0),
    /// <summary>
    /// Additional info field supplies patch MPQ filename.
    /// </summary>
    OldVersion(0x100),
    InvalidVersion(0x101),
    /// <summary>
    /// Game must be downgraded. Additional info field supplies patch MPQ filename.
    /// </summary>
    BuggedVersion(0x102),
    InvalidCDKey(0x200),
    /// <summary>
    /// Additional info field supplies name of user.
    /// </summary>
    CDKeyInUse(0x201),
    BannedCDKey(0x202),
    WrongProduct(0x203);

    private final int id;

    private BnetAuthResult(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static BnetAuthResult valueOf(int id) throws IllegalArgumentException
    {
        for (BnetAuthResult bnetAuthResult : values())
        {
            if (bnetAuthResult.getId() == id)
            {
                return bnetAuthResult;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid BnetAuthResult");
    }
}
