/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum ItemEventCause
{
    Target(0),
    Owner(1);

    private final int id;

    private ItemEventCause(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static ItemEventCause valueOf(int id) throws IllegalArgumentException
    {
        for (ItemEventCause itemEventCause : values())
        {
            if (itemEventCause.getId() == id)
            {
                return itemEventCause;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid ItemEventCause");
    }
}
