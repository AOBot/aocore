/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum PlayerRelationActionType
{
    // Neutral
    /// <summary>
    /// Player has removed Hostile
    /// </summary>
    Unhostile(4),
    // Party
    /// <summary>
    /// Player Is Asking To Party With You
    /// </summary>
    InvitedYou(5),
    /// <summary>
    /// New Player Has Joined Party
    /// </summary>
    JoinedParty(7),
    /// <summary>
    /// Your Now In This Players party ?
    /// </summary>
    JoinedYourParty(8),
    // Friendly
    /// <summary>
    /// Player has given you permission to loot his body
    /// </summary>
    AllowLoot(2),
    // UnFriendly
    /// <summary>
    /// Player has gone hostile
    /// </summary>
    Hostile(3),
    // Remove
    /// <summary>
    /// Player has left your party
    /// </summary>
    LeftParty(9),
    /// <summary>
    /// Player has canlceled the party invitation
    /// </summary>
    CanceledInvite(6),
    /// <summary>
    /// Player has canceled permission to loot his body
    /// </summary>
    CanceledLootPermission(0x0B),
    NotApplicable(0xFF);

    private final int id;

    private PlayerRelationActionType(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static PlayerRelationActionType valueOf(int id) throws IllegalArgumentException
    {
        for (PlayerRelationActionType playerRelationActionType : values())
        {
            if (playerRelationActionType.getId() == id)
            {
                return playerRelationActionType;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid PlayerRelationActionType");
    }
}
