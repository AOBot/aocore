/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum AreaLevel
{
    Abaddon(0x7d),
    AncientTunnels(0x41),
    ArcaneSanctuary(0x4a),
    ArreatPlateau(0x70),
    ArreatSummit(120),
    Barracks(0x1c),
    BlackMarsh(6),
    BloodMoor(2),
    BloodyFoothills(110),
    BurialGrounds(0x11),
    CanyonOfTheMagi(0x2e),
    CatacombsLevel1(0x22),
    CatacombsLevel2(0x23),
    CatacombsLevel3(0x24),
    CatacombsLevel4(0x25),
    Cathedral(0x21),
    CaveLevel1(9),
    CaveLevel2(13),
    ChaosSanctuary(0x6c),
    CityOfTheDamned(0x6a),
    ClawViperTempleLevel1(0x3a),
    ClawViperTempleLevel2(0x3d),
    ColdPlains(3),
    Crypt(0x12),
    CrystallinePassage(0x71),
    DarkWood(5),
    DenOfEvil(8),
    DisusedFane(0x5f),
    DisusedReliquary(0x63),
    DrifterCavern(0x74),
    DryHills(0x2a),
    DuranceOfHateLevel1(100),
    DuranceOfHateLevel2(0x65),
    DuranceOfHateLevel3(0x66),
    DurielsLair(0x49),
    FarOasis(0x2b),
    FlayerDungeonLevel1(0x58),
    FlayerDungeonLevel2(0x59),
    FlayerDungeonLevel3(0x5b),
    FlayerJungle(0x4e),
    ForgottenReliquary(0x60),
    ForgottenSands(0x86),
    ForgottenTemple(0x61),
    ForgottenTower(20),
    FrigidHighlands(0x6f),
    FrozenRiver(0x72),
    FrozenTundra(0x75),
    FurnaceOfPain(0x87),
    GlacialTrail(0x73),
    GreatMarsh(0x4d),
    HallsOfAnguish(0x7a),
    HallsOfPain(0x7b),
    HallsOfTheDeadLevel1(0x38),
    HallsOfTheDeadLevel2(0x39),
    HallsOfTheDeadLevel3(0x3c),
    HallsOfVaught(0x7c),
    HaremLevel1(50),
    HaremLevel2(0x33),
    Harrogath(0x6d),
    HoleLevel1(11),
    HoleLevel2(15),
    IcyCellar(0x77),
    InfernalPit(0x7f),
    InnerCloister(0x20),
    JailLevel1(0x1d),
    JailLevel2(30),
    JailLevel3(0x1f),
    KurastBazaar(80),
    KurastCauseway(0x52),
    KurastDocks(0x4b),
    LostCity(0x2c),
    LowerKurast(0x4f),
    LutGholein(40),
    MaggotLairLevel1(0x3e),
    MaggotLairLevel2(0x3f),
    MaggotLairLevel3(0x40),
    MatronsDen(0x85),
    Mausoleum(0x13),
    MonasteryGate(0x1a),
    MooMooFarm(0x27),
    NihlathaksTemple(0x79),
    None(0),
    OuterCloister(0x1b),
    OuterSteppes(0x68),
    PalaceCellarLevel1(0x34),
    PalaceCellarLevel2(0x35),
    PalaceCellarLevel3(0x36),
    PitLevel1(12),
    PitLevel2(0x10),
    PitOfAcheron(0x7e),
    PlainsOfDespair(0x69),
    RiverOfFlame(0x6b),
    RockyWaste(0x29),
    RogueEncampment(1),
    RuinedFane(0x62),
    RuinedTemple(0x5e),
    SewersLevel1Act2(0x2f),
    SewersLevel1Act3(0x5c),
    SewersLevel2Act2(0x30),
    SewersLevel2Act3(0x5d),
    SewersLevel3Act2(0x31),
    SpiderCave(0x54),
    SpiderCavern(0x55),
    SpiderForest(0x4c),
    StonyField(4),
    StonyTombLevel1(0x37),
    StonyTombLevel2(0x3b),
    SwampyPitLevel1(0x56),
    SwampyPitLevel2(0x57),
    SwampyPitLevel3(90),
    TalRashasTomb1(0x42),
    TalRashasTomb2(0x43),
    TalRashasTomb3(0x44),
    TalRashasTomb4(0x45),
    TalRashasTomb5(70),
    TalRashasTomb6(0x47),
    TalRashasTomb7(0x48),
    TamoeHighland(7),
    TheAncientsWay(0x76),
    ThePandemoniumFortress(0x67),
    TheWorldstoneChamber(0x84),
    TheWorldStoneKeepLevel1(0x80),
    TheWorldStoneKeepLevel2(0x81),
    TheWorldStoneKeepLevel3(130),
    ThroneOfDestruction(0x83),
    TowerCellarLevel1(0x15),
    TowerCellarLevel2(0x16),
    TowerCellarLevel3(0x17),
    TowerCellarLevel4(0x18),
    TowerCellarLevel5(0x19),
    Travincal(0x53),
    Tristram(0x26),
    UberTristram(0x88),
    UndergroundPassageLevel1(10),
    UndergroundPassageLevel2(14),
    UpperKurast(0x51),
    ValleyOfSnakes(0x2d);

    private final int id;

    private AreaLevel(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static AreaLevel valueOf(int id) throws IllegalArgumentException
    {
        for (AreaLevel areaLevel : values())
        {
            if (areaLevel.getId() == id)
            {
                return areaLevel;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid AreaLevel");
    }
}
