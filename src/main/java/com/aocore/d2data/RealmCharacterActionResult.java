/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum RealmCharacterActionResult
{
    /// <summary>
    /// The action was completed successfully.
    /// </summary>
    Success(0),
    /// <summary>
    /// Character already exists or account already has maximum number of characters (8).
    /// </summary>
    CharacterOverlap(0x14),
    /// <summary>
    /// Character name is longer than 15 characters or contains illegal characters.
    /// </summary>
    InvalidCharacterName(0x15),
    /// <summary>
    /// Invalid character name specified for action.
    /// </summary>
    CharacterNotFound(0x46),
    /// <summary>
    /// Invalid character name specified for deletion.
    /// </summary>
    CharacterDoesNotExist(0x49),
    /// <summary>
    /// The action (logon, upgrade, etc.) has failed for an unspecified reason.
    /// </summary>
    Failed(0x7A),
    /// <summary>
    /// All actions except delete are invalid on an expired character.
    /// </summary>
    CharacterExpired(0x7B),
    /// <summary>
    /// When trying to upgrade an expansion character.
    /// </summary>
    CharacterAlreadyUpgraded(0x7C);

    private final int id;

    private RealmCharacterActionResult(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static RealmCharacterActionResult valueOf(int id) throws IllegalArgumentException
    {
        for (RealmCharacterActionResult realmCharacterActionResult : values())
        {
            if (realmCharacterActionResult.getId() == id)
            {
                return realmCharacterActionResult;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid RealmCharacterActionResult");
    }
}
