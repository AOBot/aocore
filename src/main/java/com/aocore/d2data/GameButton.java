/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum GameButton
{
    CloseTrade(2),
    EnterTrade(3),
    AcceptTrade(4),
    CancelAcceptTrade(7),
    UpdateGoldOffer(8),
    CloseStash(0x12),
    WithdrawGold(0x13),
    StashGold(0x14),
    CloseCube(0x17),
    Transmute(0x18);

    private final int id;

    private GameButton(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static GameButton valueOf(int id) throws IllegalArgumentException
    {
        for (GameButton areaLevel : values())
        {
            if (areaLevel.getId() == id)
            {
                return areaLevel;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid GameButton");
    }
}
