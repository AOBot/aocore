/*
 *  Copyright 2011 derek.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.aocore.d2data;

/**
 *
 * @author derek
 */
public enum ItemUIAction
{
    /// <summary>
    /// Requesting to trade with someone (result of interacting with a player who isn't "busy").
    /// </summary>
    RequestTrade(0x00),
    /// <summary>
    /// Another player is requesting to trade with you.
    /// </summary>
    TradeRequest(0x01),
    /// <summary>
    /// Trade partner has accepted your trade offer (Acceot button goes green.)
    /// </summary>
    TradeAccepted(0x05),
    /// <summary>
    /// Unchecks all Accept buttons - sent whenever a trader changes anything in trade offer.
    /// Or might do so soon... e.g. picks up an item from inventory, clicks the offer gold button...
    /// </summary>
    UnacceptAllTrades(0x06),
    /// <summary>
    /// You don't have enough room to trade, sent instead of TradeCompleted when both accept but trade can't be performed.
    /// </summary>
    NoRoomForTrade(0x09),
    /// <summary>
    /// Trade partner doesn't have enough room to trade, sent instead of TradeCompleted when both accept but trade can't be performed.
    /// </summary>
    TraderHasNoRoom(0x0A),
    /// <summary>
    /// A player refuses trade request or cancels trade before it is completed.
    /// In the later case, this resets inventory / equipment to what they were before trade and closes the trade window.
    /// </summary>
    RefuseTrade(0x0C),
    /// <summary>
    /// Both player have accepted trade and completion is successful. Trade window closes and each player get the other player's trade buffer.
    /// </summary>
    TradeCompleted(0x0D),
    /// <summary>
    /// This turns the Accept button red, making it unclickable for a certain duration (until EnableAcceptTrade is sent.)
    /// </summary>
    DisableAcceptTrade(0x0E),
    /// <summary>
    /// Enables the Accept trade button.
    /// </summary>
    EnableAcceptTrade(0x0F),
    /// <summary>
    /// Open the stash panel.
    /// </summary>
    OpenStash(0x10),
    /// <summary>
    /// Hide the stash panel with another panel, e.g. the Horadric cube panel.
    /// The stash remains open and should (but doesn't always) get redisplayed when the hiding UI is closed...
    /// </summary>
    HideStash(0x11),
    /// <summary>
    /// Open the Horadric cube panel.
    /// </summary>
    OpenCube(0x15);

    private final int id;

    private ItemUIAction(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public static ItemUIAction valueOf(int id) throws IllegalArgumentException
    {
        for (ItemUIAction itemUIAction : values())
        {
            if (itemUIAction.getId() == id)
            {
                return itemUIAction;
            }
        }

        throw new IllegalArgumentException(id + " is not a valid ItemUIAction");
    }
}
