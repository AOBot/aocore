/*
 * Copyright 2011 derek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.aocore.awesomo.tasks;

import com.aocore.awesomo.AwesomO;
import com.aocore.awesomo.api.Task;
import com.aocore.packet.GS.GameMessage;
import com.aocore.packet.GSPacket;
import java.io.IOException;

/**
 *
 * @author derek
 */
public class GameMessageTask extends Task
{
    private String message;

    public GameMessageTask(String message)
    {
        this.message = message;
    }

    @Override
    public Task run(AwesomO awesomO) throws IOException
    {
        GSPacket packet = new GameMessage("ÿc8Awesom-Oÿc0", message);
        awesomO.sendPacket(packet);
        System.out.println("hello from bot1");
        return null;
    }
}
